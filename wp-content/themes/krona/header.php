<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package krona
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <meta name="verify-paysera" content="231c96feb447c9a88305fb0f608893c3">

    <title><?php echo wp_title("", false); ?></title>
    <?php if (get_bloginfo('description') != '') : ?>
        <meta name="description" content="<?php bloginfo('description'); ?>" />
    <?php endif; ?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162116380-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-162116380-1');
    </script>

    <script src="https://apis.google.com/js/api:client.js"></script>

    <script type="text/javascript" charset="utf-8">
        var wtpQualitySign_projectId = 164734;
        var wtpQualitySign_language  = "en";
    </script><script src="https://bank.paysera.com/new/js/project/wtpQualitySigns.js" type="text/javascript" charset="utf-8"></script>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="page" class="hfeed site">
    <?php if (get_the_ID() !== wc_get_page_id( 'checkout' )) : ?>

        <div class="header <?php echo (is_front_page() ? 'front-header' : '')?>">
            <div class="wrap">
                <div class="logo-wrapper">
                    <a href="<?php echo get_home_url(); ?>" title="<?php _e("Krona", 'krona') ?>">
                        <div class="logo"></div>
                    </a>
                </div>

                <div class="search">
                    <form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <label for="search">
                            <input type="search" class="search input"
                                   value="<?php echo get_search_query(); ?>"
                                   name="s"
                                   placeholder="<?php _e('Įrašykite, ko ieškote', 'krona'); ?>" />
                            <input type="submit" class="submit" value="" />
                        </label>
                    </form>
                    <button class="search-form-open" id="search-form-open"></button>
                </div>
                <div class="nav-cart nav-icon" id="navigation-cart">
                    <?php if (is_woocommerce_active()) :

                        global $woocommerce;
                        if (wc()->cart->get_cart_contents_count() > 0) : ?>
                            <span><?php echo wc()->cart->get_cart_contents_count(); ?></span>
                        <?php endif; ?>
                        <a href="<?php echo wc_get_cart_url(); ?>" id="cart-page-open" title="<?php _e("Pirkinių krepšelis", 'krona'); ?>">
                            <h5><?php _e("Pirkinių krepšelis", 'krona'); ?></h5>
                        </a>

                    <?php endif; ?>
                </div>
                <div class="nav-profile nav-icon">
                    <?php if ( !is_user_logged_in() ) : ?>
                        <h5><?php _e("Registruotis/ Prisijungti", 'krona'); ?></h5>
                    <?php else : ?>
                        <h5 style="width: 90px;"><?php _e("Mano profilis", 'krona'); ?></h5>
                    <?php endif; ?>
                    <div class="profile-modal">

                        <?php if ( !is_user_logged_in() ) : ?>

                            <div class="modal-row">
                                <button class="btn login-facebook" onclick="fb_login();"><?php _e('Facebook', 'krona');?></button>

                                <div id="gSignInWrapper">
                                    <div id="googleAuth" class="btn login-gmail">
                                        <span class="buttonText"><?php _e('Google', 'krona');?></span>
                                    </div>
                                </div>
                                <div id="name"></div>
                                <script>startApp();</script>
                            </div>

                            <a class="btn login-woocommerce"
                               href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>">
                                <?php _e('Prisijungti', 'krona');?>
                            </a>

                            <div class="profile-modal-register">
                                <p><?php _e('Neturite paskyros?', 'krona'); ?></p>
                                <a class="btn register-woocommerce"
                                   href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>">
                                    <?php _e('Registruotis', 'krona');?>
                                </a>
                            </div>
                        <?php endif; ?>

                        <?php if ( is_user_logged_in() ) : ?>

                        <div class="profile-shortlinks">
                            <a title=" <?php _e('Mano profilis', 'krona'); ?>"
                               href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>">
                                <?php _e('Mano profilis', 'krona'); ?>
                            </a>
                            <a title=" <?php _e('Mano užsakymai', 'krona'); ?>"
                               href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>orders">
                                <?php _e('Mano užsakymai', 'krona'); ?>
                            </a>
                            <a title=" <?php _e('Atsijungti', 'krona'); ?>"
                               href="<?php echo esc_url( wc_get_account_endpoint_url( get_option( 'woocommerce_logout_endpoint', 'customer-logout' ) ) ); ?>">
                                <?php _e('Atsijungti', 'krona'); ?>
                            </a>
                        </div>

                        <?php endif; ?>

                    </div>
                </div>
                <div class="navigation">
                    <?php wp_nav_menu( array(
                        'depth'             => 1,
                        'theme_location'    => 'primary_menu',
                        'container_class'   => 'primary-menu',
                    )); ?>

                    <div class="mobile-menu" id="mobile-menu"></div>
                </div>
            </div>
        </div>
    <?php else : ?>

    <div class="header <?php echo (is_front_page() ? 'front-header' : '')?>">
        <div class="wrap">
            <div class="logo-wrapper">
                <a href="<?php echo get_home_url(); ?>" title="<?php _e("Krona", 'krona') ?>">
                    <div class="logo"></div>
                </a>
            </div>
            <div class="info-wrap">
                <p>
                    <?php _e('Reikia pagalbos?', 'krona'); ?>
                    <span>
                        <?php the_field('footer-phone', 'option'); ?>
                    </span>
                </p>
            </div>
        </div>
    </div>

    <?php endif; ?>
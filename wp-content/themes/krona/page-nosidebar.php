<?php
/**
 * The template for displaying all pages.
 * Template Name: Full
 */
?>

<?php get_header(); ?>

	<div class="page-wrap wrap">
        <ul class="breadcrumbs">
            <?php foreach (get_theme_breadcrumbs() as $id) : ?>
                <li>
                    <a href="<?php echo get_the_permalink($id) ?>"><?php echo get_the_title($id); ?></a>
                    <span></span>
                </li>
            <?php endforeach; ?>
        </ul>

        <?php if ( !is_shop() && !is_cart() && !is_checkout() && !is_account_page() ) : ?>

            <h1><?php the_title(); ?></h1>

            <?php while ( have_posts() ) : the_post(); ?>
                <div class="post-content">
                    <?php the_content(); ?>
                </div>
            <?php endwhile; ?>

        <?php else : ?>

            <?php while ( have_posts() ) : the_post(); ?>

                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>

            <?php endwhile; ?>

        <?php endif; ?>

	</div>

    <?php if (is_cart() || is_checkout() || is_account_page()) :
        front_page_partial_content_cart() ;
    else :
        front_page_partial_content();
    endif;
    ?>

<?php get_footer(); ?>
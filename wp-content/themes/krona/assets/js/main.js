jQuery(document).ready(function( $ ) {

    /**
     * Mobile menu.
     */

    $('#mobile-menu').on('click', function() {
        var header = $('.header');
        header.hasClass('mobile-header')
            ? header.removeClass('mobile-header')
            : header.addClass('mobile-header');
    });

    $('.qna-block .question').on('click', function(){

        var answer = $(this).parent().find(".answer");

        if ( $(this).hasClass("question-open") ) {

            $(this).addClass("question-closed").removeClass("question-open");
            answer.addClass("hidden").removeClass("shown");
            return;
        }

        $('.qna-block .question').each(function() {

            var answer = $(this).parent().find(".answer");
            answer.addClass("hidden").removeClass("shown");

            $(this).addClass("question-closed").removeClass("question-open");
        });

        $(this).addClass("question-open").removeClass("question-closed");
        answer.hasClass('hidden')
            ? answer.addClass("shown").removeClass("hidden")
            : answer.addClass("hidden").removeClass("shown");
    });

    /**
     * Sticky header.
     */

    $(window).scroll(function() {

        if (($(window).scrollTop() + $(window).innerHeight()) > $('body').innerHeight()) {
            $('#wtpQualitySign_fixedCSS').addClass("adjust-position");
        } else {
            $('#wtpQualitySign_fixedCSS').removeClass("adjust-position");
        }

        var header = $('.header').height();

        if ( $(window).scrollTop() < header && $('.header').hasClass('sticky-header')) {
            $('.header').removeClass('sticky-header');
        }

        if ( $(window).scrollTop() > 0) {
            $('.header').addClass('sticky-header');
        }
    });

    $('.woocommerce').on('click', '#quantity-inc', function(e) {
        e.preventDefault();

        let val = $('.quantity input').val();
        $('.quantity input').val(parseInt(val) + 1);
        $(".woocommerce input.qty").trigger("change");
    });

    $('.woocommerce').on('click', '#quantity-dec', function(e) {
        e.preventDefault();

        let val = $('.quantity input').val();
        $('.quantity input').val( parseInt(val) > 0 ? parseInt(val) - 1 : parseInt(val));
        $(".woocommerce input.qty").trigger("change");
    });

    $('.woocommerce').on('click', '.quantity-dec', function(e) {
        e.preventDefault();

        let val = $(this).parent().find('.quantity input').val();
        $(this).parent().find('.quantity input').val( parseInt(val) > 0 ? parseInt(val) - 1 : parseInt(val));
        $(".woocommerce input.qty").trigger("change");

    }).on('click', '.quantity-inc', function(e) {
        e.preventDefault();

        let val = $(this).parent().find('.quantity input').val();
        $(this).parent().find('.quantity input').val(parseInt(val) + 1);
        $(".woocommerce input.qty").trigger("change");
    });

    $('#question-form').on('click', function(e) {

        e.preventDefault();
        $('.contact-modal').removeClass("closed").addClass("open");
    });

    $('.single_add_to_cart_button').on('click', function(e) {

        e.preventDefault();
        $('#modal-product-amount').html( $('.quantity input').val() );
        $('.cart-modal').removeClass("closed").addClass("open");
    });

    $('.modal-back, .modal-close, .continue-shopping').on('click', function(){
        $('.cart-modal').removeClass("open").addClass("closed");
        $('.contact-modal').removeClass("open").addClass("closed");
        location.reload();
    });

    $('.woocommerce-account-info-save').attr('disabled','disabled');
    $('.woocommerce-account-info-save, #privacy-policy').on('click', function(e){

        if ( $('#privacy-policy').prop( "checked" ) ) {
            $('.woocommerce-account-info-save').removeClass("disabled");
            $('.woocommerce-account-info-save').prop('disabled', false);
        } else {
            $('.woocommerce-account-info-save').attr('disabled','disabled');
            $('.woocommerce-account-info-save').addClass("disabled");
        }

    });

    gallery_image_activate( $('.woocommerce-product-gallery__image').eq(0) );

    $('.woocommerce .woocommerce-product-gallery__image').on('click', function(e){

        e.preventDefault();

        $('.woocommerce .woocommerce-product-gallery__image').each(function( key, value ) {
            $(this).removeClass('gallery_image_active');
        });

        gallery_image_activate( $(this) );
    });

    function gallery_image_activate(elemn) {

        elemn.addClass('gallery_image_active');

        $('.woocommerce-product-gallery-thumbnail').html(elemn.html());
        let image = $('.woocommerce-product-gallery-thumbnail').find('img');

        image.attr('src', image.attr('data-large_image'));
        image.attr('sizes', '(max-width: 500px) 100vw');
        image.attr('srcset', '');
        image.parent('a').attr("data-lightbox", 'galerija');
    }

    let timeout;
    $('.woocommerce').on('change', 'input.qty', function(){

        if ( timeout !== undefined ) clearTimeout( timeout );

        timeout = setTimeout(function() {
            $("[name='update_cart']").trigger("click");
        }, 100 );

        setTimeout(function() {
            $.post('/wp-admin/admin-ajax.php', { action : 'update_cart_item_count' }).done(function(result) {
                $('.nav-cart span').text(result);
            });
        }, 1000);
    });

    $('.nav-cart').on('click', function(){
        location.href = $('#cart-page-open').attr("href");
    });

    $('#form-continue-address').on('click', function(e){
        e.preventDefault();

        let pass = 4;
        $('.woocommerce-error').empty();

        $('.form-billing-tab-content input').each(function() {
            $(this).css('border-bottom', 'none');
        });

        if ($('#billing_first_name').val().length < 1) {
            print_checkout_error('Prašome užpildykite <strong>Vardas</strong> laukelį.');
            $('#billing_first_name').css('border-bottom', '2px solid #C80D3F');
            pass--;
        }

        if ($('#billing_last_name').val().length < 1) {
            print_checkout_error('Prašome užpildykite <strong>Pavardė</strong> laukelį.');
            $('#billing_last_name').css('border-bottom', '2px solid #C80D3F');
            pass--;
        }

        let regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if ( !regex.test($('#billing_email').val()) ) {
            print_checkout_error('Neteisingas el. pašto formatas');
            $('#billing_email').css('border-bottom', '2px solid #C80D3F');
            pass--;
        }

        if ($('#billing_phone').val().length < 1) {
            print_checkout_error('Prašome užpildykite <strong>Telefono</strong> laukelį.');
            $('#billing_phone').css('border-bottom', '2px solid #C80D3F');
            pass--;
        }

        if (pass === 4) {

            $('.customer-delivery').addClass('tab-open');
            setTimeout( function() {
                $('.customer-info-fields').removeClass('tab-open').addClass("validated tab-closed");
            }, 100);
        }

    });

    $('#form-continue-delivery').on('click', function(e){
        e.preventDefault();

        let pass = 4;
        $('.woocommerce-error').empty();

        $('.form-billing-tab-content input').each(function() {
            $(this).css('border-bottom', 'none');
        });

        // if ($('#billing_state').val().length < 1) {
        //     print_checkout_error('Prašome užpildykite <strong>Apskrities</strong> laukelį.');
        //     $('#billing_state').css('border-bottom', '2px solid #C80D3F');
        //     pass--;
        // }

        if ($('#billing_city').val().length < 1) {
            print_checkout_error('Prašome užpildykite <strong>Miesto</strong> laukelį.');
            $('#billing_city').css('border-bottom', '2px solid #C80D3F');
            pass--;
        }

        if ($('#billing_address_1').val().length < 5) {
            print_checkout_error('Prašome užpildykite <strong>Gatvės ir namo numerio</strong> laukelį.');
            $('#billing_address_1').css('border-bottom', '2px solid #C80D3F');
            pass--;
        }

        if ($('#billing_postcode').val().length < 5 || $('#billing_postcode').val().length > 5) {
            print_checkout_error('Prašome užpildykite <strong>Pašto kodo</strong> laukelį.');
            $('#billing_postcode').css('border-bottom', '2px solid #C80D3F');
            pass--;
        }

        if (pass === 4) {

            $('.delivery-method').addClass('tab-open');
            setTimeout( function() {
                $('.customer-delivery').removeClass('tab-open').addClass("validated tab-closed");
            }, 100);
        }

    });

    $('#form-continue-payment').on('click', function(e){
        e.preventDefault();

        let pass = 1;

        if ($('input.shipping_method:checked').length === 0) {

            print_checkout_error('Prašome pasirikite <strong>Pristatymo būdą.</strong>');
            pass--;
        }

        if (pass === 1 ) {
            $('.payment-method').addClass('tab-open');
            setTimeout( function() {
                $('.delivery-method').removeClass('tab-open').addClass("validated tab-closed");
            }, 100);
        }
    });

    $('.form-billing-tab-wrap').on('click', function(e){

        let content = $(this).find('.form-billing-tab-content');
        if ( content.hasClass('validated') ) {
            content.hasClass("tab-closed")
                ? content.addClass('tab-open').removeClass('tab-closed')
                : content.removeClass('tab-open').addClass('tab-closed') ;
        }

    });

    $('.form-billing-tab-wrap #place_order').on('click', function(e){

        if ( !$('#accept-rules-and-privacy').is(':checked') )
        {
            $('.place-order .woocommerce-NoticeGroup-checkout').html(
                '<ul class="woocommerce-error" role="alert" style="padding: 10px 0 !important;">' +
                '<li>Prašome sutikti su sąlygomis ir taisyklėmis ir privatumo politika.</li>' +
                '</ul>' +
                '</div>'
            );
            e.preventDefault();
        }

    });

    $('.ajax_add_to_cart').on('click', function(e){
        setTimeout( function() {
            location.reload();
        }, 1000);
    });

    function print_checkout_error($error) {

        if ( $('form.woocommerce-checkout .woocommerce-NoticeGroup').length === 0 ) {
            $('form.woocommerce-checkout').prepend(
                '<div class="woocommerce-NoticeGroup woocommerce-NoticeGroup-checkout">' +
                '<ul class="woocommerce-error" role="alert"></ul>' +
                '</div>'
            );
        }

        $('.woocommerce-error').append(
            '<li>' + $error + '</li>'
        );
    }

    $('.woocommerce-form-login-toggle').on('click', function(e){

        let div = $('.form-user');
        if ( $('#checkout-customer:checked').length ) {
            div.addClass('login-open')
            $('.customer-info-fields').addClass("hidden");
        } else {
            div.removeClass('login-open')
            $('.customer-info-fields').removeClass("hidden");
        }

        let div2 = $('.form-user-button');
        if ( $('#checkout-customer:checked').length ) {
            div2.addClass('login-open-button')
        } else {
            div2.removeClass('login-open-button')
        }

    });

    $('.ajax-loginuser').on('click', function(e){
        e.preventDefault();

        let url = $('.form-user button').attr("data-action");

        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: url,
            data: {
                'action': 'ajaxlogin',
                'username': $('.form-user-username input').val(),
                'password': $('.form-user-password input').val(),
                'security': $('.form-user #security').val()
            },
            success: function(data)
            {
                if (data.loggedin == true)location.reload();
            }
        });
    });

    $('.product').on('touchstart', function() {
        $(this).removeClass("unhover");
    });

    $('.product .close-product').on('click', function() {
        $(this).parent().addClass("unhover");
    });

    $('body').on('click', '.close-qualitySign', function() {
        $('#wtpQualitySign_popupCSS').css();
    });
    $('#wtpQualitySign_popupCSS').prepend('<div class="close-qualitySign"></div>');

    $('body').on('mouseover', '.product-category-sidebar .sub-menu li', function() {
        $(this).next('ul').addClass("visible-menu-item");
    });

    $('body').on('mouseout', '.product-category-sidebar .sub-menu li', function() {
        $(this).next('ul').removeClass("visible-menu-item");
    });

    $('body').on('click', '#shipping_method label', function(e){
        $(this).parent().find('input').click();
    });

    $('body').on('click', function(e){

        if ( $(e.target).hasClass('search') || $(e.target).hasClass('search-form')) {
            return;
        }

        let search = $('.header .search');
        if (search.hasClass("search-open")) {
            search.addClass('search-closing');
            search.removeClass('search-open');

            setTimeout( function() {
                search.removeClass('search-closing');
            }, 100);
        }

    });

    $( document ).ajaxComplete( function() {
        if ( $( 'body' ).hasClass( 'woocommerce-checkout' ) || $( 'body' ).hasClass( 'woocommerce-cart' ) ) {
            $( 'html, body' ).stop();
        }
    } );

    $('#search-form-open').on('click', function(e){

        setTimeout( function() {

            let search = $('.header .search');
            if ( !search.hasClass("search-open") ) {
                search.addClass('search-open');
            }

        }, 10);

    });

    $('body').on('click', function(e){
        let div = $('.woocommerce-filter-column');
        div.removeClass('filter-column-open');
    });

    $('#filter-collapse').on('click', function(e){

        setTimeout(function() {
            let div = $('.woocommerce-filter-column');
            if ( div.hasClass('filter-column-open') ) {
                div.removeClass('filter-column-open');
            } else {
                div.addClass('filter-column-open');
            }
        }, 10);
    });

});

(function () {

    let navigationCart = document.getElementById("navigation-cart");
    if (typeof(navigationCart) != 'undefined' && navigationCart != null)
    {
        let request = new XMLHttpRequest();
        request.open('POST', '/wp-admin/admin-ajax.php', true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.onload = function () {
            if (this.status >= 200 && this.status < 400) {
                if (this.response) {
                    navigationCart.innerHTML = this.response;
                }
            }
        };
        request.send('action=TwsJNSy2Ap_update_cart_totals');
    }


    /**
     * Lightbox for gallery images.
     */

    let gallery = document.querySelectorAll('.wp-block-gallery img');
    for (let i = 0; i < gallery.length; i++){

        let src = gallery[i].getAttribute("data-full-url");
        let href = document.createElement('a');
        href.setAttribute("href", src);
        href.setAttribute("data-lightbox", 'galerija');

        gallery[i].parentNode.insertBefore(href, gallery[i]);
        href.appendChild(gallery[i]);
    }

    /**
     * Carousel using Glide.
     */

    let carousel_el = document.getElementById('carousel');
    if (typeof(carousel_el) != 'undefined' && carousel_el != null) {

        let carousel = new Glide('#carousel', {
            type: 'slider',
            gap: 0,
            perView: 1,
            hoverpause: true,
            autoplay: 5000,
        });
        carousel.mount();
    }

    let carousel_sales = document.getElementById('sale-carousel');
    if (typeof(carousel_sales) != 'undefined' && carousel_sales != null) {

        let carousel = new Glide('#sale-carousel', {
            type: 'slider',
            gap: 0,
            perView: 4,
            hoverpause: true,
            breakpoints: {
                1300: {
                    perView: 3
                },
                900: {
                    perView: 2
                },
                450: {
                    perView: 1
                }
            }
        });
        carousel.mount();
    }

    /**
     * Mobile menu show/hide on resize.
     */

    window.addEventListener('resize', function() {
        if (window.innerWidth > 800) {
            document.querySelector( '.header' ).classList.remove( 'mobile-header' );
        }
    });

    let add_to_cart = document.getElementsByClassName('single_add_to_cart_button')[0];
    if (typeof(add_to_cart) != 'undefined' && add_to_cart!= null) {
        add_to_cart.onclick = function(e) {
            e.preventDefault();

            let request = new XMLHttpRequest();
            request.open('POST', '/wp-admin/admin-ajax.php', true);
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

            request.onload = function() {
                if (this.status >= 200 && this.status < 400) {
                    if (this.response) {
                       document.getElementsByClassName('cart-info')[0].innerHTML = this.response;
                    }
                }
            };
            request.send('action=theme_product_add_to_cart&id=' + add_to_cart.value );
        }
    }

    /**
     * Lazy load images.
     */

    let lazy = [];

    registerListener('load', lazyLoad);
    registerListener('scroll', lazyLoad);
    registerListener('resize', lazyLoad);

    function registerListener(event, func) {
        if (window.addEventListener) {
            window.addEventListener(event, func)
        } else {
            window.attachEvent('on' + event, func)
        }
    }

    function lazyLoad(){

        lazy = document.getElementsByClassName('lazy-image');

        for (let i = 0; i < lazy.length; i++){
            if (isInViewport(lazy[i])) {

                if (lazy[i].getAttribute('data-src')){
                    if (lazy[i].tagName === "IMG") {

                        lazy[i].src = lazy[i].getAttribute('data-src');
                        lazy[i].removeAttribute('data-src');

                    } else {

                        lazy[i].style['background-image'] = 'url("' + lazy[i].getAttribute('data-src') + '")';
                        lazy[i].removeAttribute('data-src');
                    }
                }
            }
        }

        lazy = Array.prototype.filter.call(lazy, function(l){
            return l.getAttribute('data-src');
        });
    }

    function isInViewport(el){

        var rect = el.getBoundingClientRect();
        return (
            rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.left <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }


    select("woocommerce-ordering");
    function select(selector) {

        var x, i, j, selElmnt, a, b, c;
        x = document.getElementsByClassName(selector);

        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];

            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            a.setAttribute("id", selElmnt.options[selElmnt.selectedIndex].value);

            x[i].appendChild(a);

            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            b.setAttribute("style", "width:" + a.getBoundingClientRect().width + "px");
            for (j = 0; j < selElmnt.length; j++) {

                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.setAttribute("id", selElmnt.options[j].value);

                if (selElmnt.options[j].selected) continue;

                c.addEventListener("click", function (e) {

                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            h.setAttribute("id", this.getAttribute("id"));
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            break;
                        }
                    }
                    h.click();
                    history.replaceState(null, null,
                        replaceUrlParam(window.location.href, 'orderby', e.target.id)
                    );
                    location.reload();
                });
                b.appendChild(c);
            }
            x[i].appendChild(b);

            a.addEventListener("click", function (e) {

                e.stopPropagation();
                closeAllSelect(this);

                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }

        function closeAllSelect(elmnt) {

            var x, y, i, arrNo = [];
            x = document.getElementsByClassName("select-items");
            y = document.getElementsByClassName("select-selected");
            for (i = 0; i < y.length; i++) {
                if (elmnt == y[i]) {
                    arrNo.push(i)
                } else {
                    y[i].classList.remove("select-arrow-active");
                }
            }
            for (i = 0; i < x.length; i++) {
                if (arrNo.indexOf(i)) {
                    x[i].classList.add("select-hide");
                }
            }
        }

        document.addEventListener("click", closeAllSelect);
    }

    function replaceUrlParam(url, paramName, paramValue)
    {
        if (paramValue == null) paramValue = '';

        var pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');
        if (url.search(pattern)>=0) {
            return url.replace(pattern,'$1' + paramValue + '$2');
        }
        url = url.replace(/[?#]$/,'');
        return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;
    }

})();
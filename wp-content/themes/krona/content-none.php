<?php
/**
 * The template part for displaying a message that posts cannot be found.
 */
?>

<div class="no-results not-found wrap">
    <?php if ( is_search() ) : ?>

        <h1><?php esc_html_e( 'Dėja, nieko nerasta', 'krona' ); ?></h1>
        <p><?php esc_html_e( 'Pagal įvestus raktažodžius rezultatų nerasta.', 'krona' ); ?></p>

    <?php else : ?>

        <h1><?php esc_html_e( 'Dėja, toks puslapis nerastas', 'krona' ); ?></h1>
        <p><?php esc_html_e( 'Puslapis buvo pašalintas arba perkeltas kitur.', 'krona' ); ?></p>

    <?php endif; ?>
</div>
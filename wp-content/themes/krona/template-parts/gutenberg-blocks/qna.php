<?php
/**
 * Manufacturers block template.
 */

$id = 'qna-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'qna-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">

    <?php if( have_rows('qna-repeater') ):
        while ( have_rows('qna-repeater') ) : the_row(); ?>

            <div class="entry-qna">
                <div class="question">
                    <h3><?php the_sub_field('qna-block-question'); ?></h3>
                </div>
                <div class="answer hidden">
                    <?php the_sub_field('qna-block-answer'); ?>
                </div>
            </div>

        <?php endwhile;
    endif; ?>
</div>
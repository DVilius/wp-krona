<?php
/**
 * Product Carousel block template.
 */

$id = 'product-carousel-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'product-carousel-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$slides = count(get_field('product-carousel'));
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">

        <div class="product-category-sidebar">
            <ul class="sub-menu">
                <?php
                $parent = 0;
                $product_categories = get_terms(array(
                    'taxonomy'   => "product_cat",
                    'orderby'    => "menu_order",
                    'order'      => "ASC",
                    'hide_empty' => false,
                    'exclude'    => array(15),
                ));

                foreach ($product_categories as $cat) :
                    if ($cat->parent == 0) :
                        $parent = $cat->term_id;
                        $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                        $image = wp_get_attachment_url( $thumbnail_id );
                    ?>
                        <li class="product-cat-<?php echo $cat->slug ?>">
                            <a href="<?php echo get_term_link($cat->term_id) ?>">
                                <img src="<?php echo $image?>" alt="<?php echo $cat->name ?>"/>
                                <?php echo $cat->name ?>
                            </a>
                        </li>
                        <ul class="sub-menu">
                            <?php
                            $i = 4;
                            foreach ($product_categories as $cat_2) :
                                if ($cat_2->parent == $parent) : ?>
                                    <li class="product-cat-<?php echo $cat_2->slug ?><?php echo $i < 1 ? ' menu-invisible' : ''?>">
                                        <a href="<?php echo get_term_link($cat_2->term_id) ?>"><?php echo $cat_2->name ?></a>
                                    </li>
                                    <?php $i--; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="product-carousel" id="carousel">

            <div class="product-carousel-track" data-glide-el="track">
                <ul class="product-carousel-wrap">

                    <?php if ( have_rows('product-carousel') ) :
                        while ( have_rows('product-carousel') ) : the_row();

                            $image = get_sub_field('product-carousel-image');
                            $link = get_sub_field('product-carousel-product');
                            $current++;
                            ?>
                            <li class="carousel-slide" style="background-image: url(<?php echo $image ? $image['url'] : ''; ?>)">
                                <div class="wrap-content">

                                    <h3><?php the_sub_field('product-carousel-subtitle'); ?></h3>
                                    <h2><?php the_sub_field('product-carousel-title'); ?></h2>

                                    <?php if ( get_sub_field('product-carousel-desc') ) : ?>
                                        <div class="text">
                                            <?php the_sub_field('product-carousel-desc'); ?>
                                        </div>
                                    <?php endif; ?>
                                </div>

                                <a href="<?php echo $link ? get_permalink($link[0]->ID) : ''; ?>">
                                    <?php _e("Žiūrėti prekę"); ?>
                                </a>

                                <div class="glide__bullets" data-glide-el="controls[nav]">
                                    <?php for ($i = 0; $i < $slides; $i++) : ?>
                                        <button class="glide__bullet" data-glide-dir="=<?php echo $i; ?>"></button>
                                    <?php endfor; ?>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>

        </div>
        <div class="clear"></div>

    </div>
</div>
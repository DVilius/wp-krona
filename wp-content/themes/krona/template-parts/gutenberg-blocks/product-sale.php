<?php
/**
 * Product Sale block template.
 */

$id = 'product-sale-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'product-sale-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">

        <h2><?php _e("Išpardavimas"); ?></h2>
        <div class="product-list-wrap">
            <?php
            $query = new WP_Query(array(
                'post_type'      => 'product',
                'posts_per_page' => 4,
                'post_status' => 'publish',
                'meta_query' => array(
                    'relation' => 'OR',
                    array (
                        'key'           => '_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    ),
                    array (
                        'key'           => '_min_variation_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    )
                )
            ));

            //            if ( $query->have_posts() ) : ?>
            <!--                --><?php //if ( $query->found_posts > 4) : ?>

            <div class="sale-carousel" id="sale-carousel">
                <div class="sale-carousel-track" data-glide-el="track">
                    <ul class="sale-carousel-wrap">
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                            <?php wc_get_template_part( 'content', 'product' ); ?>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>

            <!--                --><?php //else : ?>
            <!---->
            <!--                    --><?php //while ( $query->have_posts() ) : $query->the_post(); ?>
            <!--                        --><?php //wc_get_template_part( 'content', 'product' ); ?>
            <!--                    --><?php //endwhile; ?>
            <!---->
            <!--                --><?php //endif; ?>
            <!--            --><?php //endif; ?>
        </div>

    </div>
</div>
<?php
/**
 * Product Sale block template.
 */

$id = 'product-category-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'product-category-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$term = get_field('product-category-block-cat');
$image = get_field('product-category-block-back');
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">

        <div class="text-wrap">
            <h2><?php the_field('product-category-block-title'); ?></h2>
            <a href="<?php echo get_term_link($term, 'product_cat'); ?>"><?php _e("Visos kategorijos prekės"); ?></a>
        </div>

        <div class="block-wrap" style="border-color: <?php the_field('product-category-border-color'); ?> !important;">
            <?php
            $products = new WP_Query(array(
                'post_type'             => 'product',
                'post_status'           => 'publish',
                'posts_per_page'        => 6,
                'tax_query'             => array(
                    array(
                        'taxonomy'      => 'product_cat',
                        'field'         => 'term_id',
                        'terms'         => $term,
                        'operator'      => 'IN'
                    ),
                )
            ));
            ?>
            <div class="product-list-wrap">
                <div class="category-column">
                    <?php
                    $terms = 0;
                    $product_categories = get_terms(array(
                        'taxonomy'   => "product_cat",
                        'orderby'    => "menu_order",
                        'order'      => "ASC",
                        'hide_empty' => false,
                        'exclude'    => array(15),
                    ));
                    ?>

                    <ul>
                        <?php foreach ($product_categories as $cat) :
                            if ($cat->parent == $term) : ?>
                            <?php $terms++; ?>
                                <li class="product-cat-<?php echo $cat->slug ?>" data-color="<?php the_field('product-category-border-color'); ?>">
                                    <a href="<?php echo get_term_link($cat->term_id) ?>"><?php echo $cat->name ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>

                    <div class="back">
                        <img src="<?php echo $image ? $image['url'] : ''; ?>" alt="" />
                    </div>

                </div>
                <div class="product-column">
                    <?php
                    if ( $products->have_posts() ) : ?>
                        <?php while ( $products->have_posts() ) : $products->the_post(); ?>
                            <?php wc_get_template_part( 'content', 'product' ); ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
</div>
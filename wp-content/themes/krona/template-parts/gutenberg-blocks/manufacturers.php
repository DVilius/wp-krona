<?php
/**
 * Manufacturers block template.
 */

$id = 'manufacturers-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'manufacturers-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">

        <ul class="manufacturers-wrap">
            <li>
                <h4><?php _e("Atstovaujami gamintojai", "krona"); ?></h4>
            </li>
            <?php if ( have_rows('manufacturers') ): ?>
                <?php while ( have_rows('manufacturers') ) : the_row();

                    $image = get_sub_field("manufacturers-block-image");
                    $link = get_sub_field("manufacturers-block-link");
                ?>

                    <li>
                        <a href="<?php echo $link; ?>" target="_blank">
                            <img src="<?php echo $image ? $image['url'] : '' ?>"
                                 alt="<?php echo $image ? $image['alt'] : '' ?>" />
                        </a>
                    </li>

                <?php endwhile; ?>

            <?php endif; ?>
        </ul>

    </div>
</div>
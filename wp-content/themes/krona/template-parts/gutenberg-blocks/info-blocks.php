<?php
/**
 * Info blocks template.
 */

$id = 'info-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

$className = 'info-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="wrap">

        <ul class="info-wrap">
            <?php if ( have_rows('info-block') ): ?>
                <?php while ( have_rows('info-block') ) : the_row();

                    $image = get_sub_field("info-block-image");
                    $link = get_sub_field("info-block-url");
                ?>

                    <li>
                        <a href="<?php echo $link ? $link['url'] : ''; ?>">
                            <img src="<?php echo $image ? $image['url'] : '' ?>"
                                 alt="<?php echo $image ? $image['alt'] : '' ?>" />
                            <h4><?php the_sub_field("info-block-title"); ?></h4>
                            <?php the_sub_field("info-block-text"); ?>
                        </a>
                    </li>

                <?php endwhile; ?>

            <?php endif; ?>
        </ul>

    </div>
</div>
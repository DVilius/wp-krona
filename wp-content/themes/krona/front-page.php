<?php
/**
 * The template for displaying front page.
 */

get_header(); ?>

    <div class="front-content">
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>

                <?php the_content(); ?>

            <?php endwhile; ?>
        <?php endif; ?>
    </div>

<?php
get_footer();
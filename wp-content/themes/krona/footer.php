<?php
/**
 * The template for displaying the footer.
 */
?>

<?php if (get_the_ID() !== wc_get_page_id( 'checkout' )) : ?>

	<footer>
        <div class="wrap">
            <ul>
                <li>
                    <h4><?php _e("Susisiekite", "krona"); ?></h4>
                    <a class="phone" href="tel:<?php the_field('footer-phone', 'option'); ?>">
                        <?php the_field('footer-phone', 'option'); ?>
                    </a>
                    <a class="mail" href="mailto:<?php the_field('footer-mail', 'option'); ?>">
                        <?php the_field('footer-mail', 'option'); ?>
                    </a>
                    <a class="address" href="https://www.google.com/maps/place/<?php the_field('footer-address', 'option'); ?>">
                        <?php the_field('footer-address', 'option'); ?>
                    </a>

                    <div class="payment-types">
                        <img height="50" width="50" alt="visa" src="<?php echo get_template_directory_uri() . '/assets/images/visa.svg'?>">
                        <img height="50" width="50" alt="mastercard" src="<?php echo get_template_directory_uri() . '/assets/images/mastercard.svg'?>">
                    </div>
                </li>
                <li>
                    <h4><?php _e("Kategorijos", "krona"); ?></h4>
                    <?php
                    $product_categories = get_terms(array(
                        'taxonomy'   => "product_cat",
                        'orderby'    => "menu_order",
                        'order'      => "ASC",
                        'hide_empty' => false,
                        'exclude'    => array(15),
                    ));
                    ?>

                    <ul class="product-categories">
                        <?php foreach ($product_categories as $cat) :
                            if ($cat->parent == 0) : ?>
                                <li class="product-cat-<?php echo $cat->slug ?>">
                                    <a href="<?php echo get_term_link($cat->term_id) ?>"><?php echo $cat->name ?></a>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li>
                    <h4><?php _e("Prekės", "krona"); ?></h4>
                    <?php wp_nav_menu( array(
                        'depth'             => 1,
                        'theme_location'    => 'product_menu',
                        'container_class'   => 'product_menu',
                    )); ?>
                </li>
                <li>
                    <h4><?php _e("Klientams", "krona"); ?></h4>
                    <?php wp_nav_menu( array(
                        'depth'             => 1,
                        'theme_location'    => 'client_menu',
                        'container_class'   => 'client_menu',
                    )); ?>
                </li>
                <li>
                    <h4><?php _e("Apie Krona", "krona"); ?></h4>
                    <?php wp_nav_menu( array(
                        'depth'             => 1,
                        'theme_location'    => 'about_menu',
                        'container_class'   => 'about_menu',
                    )); ?>
                </li>
                <li class="extra">

                    <ul class="socials">
                        <li>
                            <a href="<?php the_field('footer-facebook', 'option'); ?>" title="Facebook" target="_blank">
                                <img src="<?php echo get_template_directory_uri() . '/assets/images/facebook.svg'?>"
                                     alt="Facebook"/>
                            </a>
                        </li>
                        <li>
                            <a href="<?php the_field('footer-instagram', 'option'); ?>" title="Instagram" target="_blank">
                                <img src="<?php echo get_template_directory_uri() . '/assets/images/instagram.svg'?>"
                                     alt="Instagram"/>
                            </a>
                        </li>
                    </ul>

                    <p class="copyright"><?php _e('© 2019 UAB „Krona“', 'krona'); ?></p>
                    <p><a target="_blank" href="https://www.cpartner.lt/lt/interneto-svetainiu-kurimas/"
                          title="Interneto svetainiu kūrimas">
                            <?php _e('Sukurė', 'krona'); ?>
                        </a>
                        <a target="_blank" href="https://www.cpartner.lt/">
                            <img src="<?php echo get_template_directory_uri() . '/assets/images/cp.svg'?>" alt="Creative Partner"/>
                        </a>
                    </p>
                </li>
            </ul>

            <div class="question-us-wrap">
                <a id="question-form" href=""><?php _e('Klauskite mūsų', 'krona'); ?></a>
            </div>

        </div>
	</footer>

<?php endif; ?>

</div><!-- #page -->

<?php wp_footer(); ?>

<?php /* Contact form modal */ ?>
<div class="contact-modal closed">
    <div class="modal-content">
        <div class="modal-close">x</div>
        <div class="modal-body">
            <?php echo do_shortcode('[contact-form-7 id="362" title="Kontaktai"]'); ?>
        </div>
    </div>
    <div class="modal-back"></div>
</div>

<?php /* Product add to cart modal */ ?>
<?php if (is_single() && get_post_type() === "product") : ?>
    <div class="cart-modal closed">
        <div class="modal-content">
            <div class="modal-head">
                <h3><?php _e("Prekė sėkmingai pridėta į krepšelį", "strofront"); ?></h3>
                <div class="modal-close">x</div>
            </div>
            <div class="modal-body">
                <?php global $product; ?>

                <div class="row">
                    <div class="col-6">
                        <?php the_post_thumbnail(); ?>
                    </div>
                    <div class="col-6">
                        <?php woocommerce_template_single_title(); ?>
                        <p class="price">
                            <?php if ( $product->is_on_sale() ) : ?>
                                <ins>
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                                       <?php echo $product->get_sale_price(); ?>
                                    </span>
                                </ins>
                                <del>
                                    <span class="woocommerce-Price-amount amount">
                                        <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                                         <?php echo $product->get_regular_price(); ?>
                                    </span>
                                </del>
                            <?php else : ?>
                                <span class="woocommerce-Price-amount amount">
                                    <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                                    <?php echo $product->get_regular_price(); ?>
                                </span>
                            <?php endif; ?>
                        </p>
                        <p class="product-amount">
                            <?php _e("Kiekis:", "krona"); ?>
                            <span id="modal-product-amount"></span>
                        </p>
                    </div>
                </div>

                <div class="cart-info"></div>
            </div>

            <div class="modal-footer">
                <a id="continue-shopping" class="continue-shopping">
                    <?php _e("Tęsti apsipirkimą"); ?>
                </a>
                <a id="open-cart" class="open-cart" href="<?php echo wc_get_cart_url(); ?>">
                    <?php _e("Pereiti prie apmokėjimo"); ?>
                </a>
            </div>
        </div>
        <div class="modal-back"></div>
    </div>
<?php endif; ?>

</body>
</html>

<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

$queried_object = get_queried_object();

if ( woocommerce_product_loop() ) {

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

    $queried_object = get_queried_object();;
    if ($queried_object->parent == 0) {

        $parent = 0;
        $product_categories = get_terms(array(
            'taxonomy'   => "product_cat",
            'orderby'    => "menu_order",
            'order'      => "ASC",
            'hide_empty' => false,
            'exclude'    => array(15),
        ));

        foreach ($product_categories as $cat) :
            if ($cat->parent == 0 && $queried_object->term_id == $cat->term_id
                || $cat->parent == 0 && $queried_object->term_id == null) :
                $parent = $cat->term_id;
                $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                $image = wp_get_attachment_url( $thumbnail_id );

                foreach ($product_categories as $cat_2) :
                    if ($cat_2->parent == $parent) : ?>
                        <li class="product product-category">

                            <span class="close-product">x</span>
                            <a href="<?php echo get_term_link($cat_2->term_id) ?>">

                                <?php echo wp_get_attachment_image(get_field('category-image', $cat_2), 'full'); ?>
                                <h4><?php echo $cat_2->name ?></h4>

                                <div class="hidden-product-info">
                                    <a href="<?php echo get_term_link($cat_2->term_id) ?>" class="button" rel="nofollow"><?php _e("Žiūrėti prekes", "krona"); ?></a>
                                </div>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>

            <?php endif; ?>
        <?php endforeach; ?>

<?php
    } else {

        $product_query = new WP_Query(theme_product_archive_query_args());
        if ( wc_get_loop_prop( 'total' ) ) :
            while ( $product_query->have_posts() ) : $product_query->the_post();

                /**
                 * Hook: woocommerce_shop_loop.
                 *
                 * @hooked WC_Structured_Data::generate_product_data() - 10
                 */
                do_action( 'woocommerce_shop_loop' );

                wc_get_template_part( 'content', 'product' );
            endwhile;
        endif;

    }

	woocommerce_product_loop_end();

} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

front_page_partial_content();

get_footer( 'shop' );

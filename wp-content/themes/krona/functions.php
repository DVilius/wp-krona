<?php

require 'inc/gutenberg-functions.php';
require 'inc/woocommerce-functions.php';

/*
 * Add support for WordPress features.
 **/
add_theme_support( 'post-thumbnails' );
add_theme_support( 'custom-logo' );
add_theme_support('woocommerce');

/*
 * Load theme styles/scripts.
 **/
function enqueue_theme_scripts() {

    wp_enqueue_style( 'theme-style', get_template_directory_uri() .'/style.css', array(), '1.0' );

    wp_enqueue_script( 'glide', get_template_directory_uri() .'/assets/js/glide.min.js' );
    wp_enqueue_script( 'main', get_template_directory_uri() .'/assets/js/main.js', array('jquery', 'glide') );
    wp_enqueue_script( 'lighbox', get_template_directory_uri() .'/assets/js/lightbox.js' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_theme_scripts' );

/*
 * Upload support for SVG images.
 **/
function theme_add_supported_file_types_to_uploads($file_types) {
    $new_file_types = array();
    $new_file_types['svg'] = 'image/svg+xml';

    $file_types = array_merge($file_types, $new_file_types);
    return $file_types;
}
add_action('upload_mimes', 'theme_add_supported_file_types_to_uploads');

/*
 * Display svg media images as thumbnails.
 **/
function theme_display_svg_media_thumbnails($response, $attachment, $meta) {
    if ($response['type'] === 'image' && $response['subtype'] === 'svg+xml' && class_exists('SimpleXMLElement')) {
        try {
            $path = get_attached_file($attachment->ID);

            if (@file_exists($path)) {

                $svg = new SimpleXMLElement(@file_get_contents($path));
                $src = $response['url'];
                $width = (int) $svg['width'];
                $height = (int) $svg['height'];

                //media gallery
                $response['image'] = compact( 'src', 'width', 'height' );
                $response['thumb'] = compact( 'src', 'width', 'height' );

                //media single
                $response['sizes']['full'] = array(
                    'height'        => $height,
                    'width'         => $width,
                    'url'           => $src,
                    'orientation'   => $height > $width ? 'portrait' : 'landscape',
                );
            }
        } catch(Exception $e) {}
    }

    return $response;
}
add_filter('wp_prepare_attachment_for_js', 'theme_display_svg_media_thumbnails', 10, 3);

function console_log($data) {
    echo '<script>';
    echo 'console.log('. json_encode( $data ) .')';
    echo '</script>';
}

/*
 * Register navigation menus.
 **/
function theme_register_nav_menu() {
    register_nav_menus(array(
        'primary_menu'  => __( 'Pagrindinis Meniu', 'storefront' ),
        'about_menu'  => __( 'Apie Meniu', 'storefront' ),
        'product_menu'  => __( 'Prekės Meniu', 'storefront' ),
        'client_menu'  => __( 'Klientams Meniu', 'storefront' ),
    ));
}
add_action( 'after_setup_theme', 'theme_register_nav_menu', 0 );

function theme_append_primary_menu_items($items, $args) {

    $old_items = $items;
    $items =
        '<li class="catalog" id="catalog">' .
           '<a href="'. get_permalink( wc_get_page_id( 'shop' ) ) .'">'. __("Prekių katalogas", "krona") .'</a>' .
            '<div class="catalog-categories-wrap"><div class="catalog-content">' .
            '<ul class="sub-menu">';

            $product_categories = get_terms(array(
                'taxonomy'   => "product_cat",
                'orderby'    => "menu_order",
                'order'      => "ASC",
                'hide_empty' => false,
                'exclude'    => array(15),
            ));

            foreach ($product_categories as $cat) :
                if ($cat->parent == 0) :
                    $parent = $cat->term_id;
                    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
                    $image = wp_get_attachment_url( $thumbnail_id );

                    $items .=
                        '<li class="product-cat-'. $cat->slug . '">' .
                        '<a href="' . get_term_link($cat->term_id) . '">' .
                            '<img src="'. $image .'" alt="'. $cat->name .'"/>' .
                             $cat->name .
                        '</a>' .
                    '</li>';

                    $items .= '<ul class="sub-menu">';
                    foreach ($product_categories as $cat_2) :
                        if ($cat_2->parent == $parent) :
                            $items .=
                                '<li class="product-cat-' .$cat_2->slug . '">' .
                                    '<a href="'. get_term_link($cat_2->term_id) . '">'.  $cat_2->name . '</a>' .
                                '</li>';
                        endif;
                    endforeach;
                    $items .= '</ul>';
            endif;
            endforeach;

    $items .= '</ul></div></div></li>';

    $items .= $old_items;
    return $items;
}
add_filter('wp_nav_menu_'. theme_get_menu_slug('primary_menu') .'_items', 'theme_append_primary_menu_items', 10, 2);

function add_navigation_menu_icons( $items, $args ) {

    foreach( $items as $item ) {
        if ($icon = get_field('menu-icon', $item)) {
            $_icon = '<i style="background: url('. $icon["url"] .')" class="icon-container"></i>';
            $item->title = $_icon . $item->title;
        }
    }

    return $items;
}
add_filter('wp_nav_menu_objects', 'add_navigation_menu_icons', 10, 2);

function theme_get_menu_slug($name) {
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locations[$name]);
    return $menu->slug;
}

/*
 * Add custom option page.
 **/
if ( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Temos Nustatymai',
        'menu_title'	=> 'Temos Nustatymai',
        'menu_slug' 	=> 'theme-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false,
        'icon_url' 		=> 'dashicons-flag',
        'position' 		=> 100
    ));
}

/*
 * Lock plugin version.
 **/
function remove_update_notification($value) {
    unset($value->response["advanced-custom-fields-pro-master/acf.php"]);
    return $value;
}
add_filter('site_transient_update_plugins', 'remove_update_notification');

/*
 * Disable menu item link.
 **/
function theme_filter_nav_menu_objects( $items, $args ) {

    foreach ($items as &$item) {
        $disabled = get_field('meniu-item-disabled', $item);
        if ($disabled) {
            array_push($item->classes, "menu-item-disabled");
        }
    }

    return $items;
}
add_filter('wp_nav_menu_objects', 'theme_filter_nav_menu_objects', 10, 2);

/*
 * Breadcrumbs.
 **/
function get_theme_breadcrumbs()
{
    $breadcrumbs = array();

    if ( is_home() ) {
        array_push($breadcrumbs, get_option('page_for_posts'));

    } else if ( is_page() ) {
        array_push($breadcrumbs, get_the_ID());

    } else if ( is_single() ) {

        array_push($breadcrumbs, get_the_ID());

        if (get_post_type() === "learning") {
            $service_archive = get_pages(array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'hierarchical' => 0,
                'meta_value' => 'lessons.php'
            ));

            array_push($breadcrumbs, $service_archive[0]->ID);
        }

        if (get_post_type() === "post") {
            array_push($breadcrumbs, get_option('page_for_posts'));
        }
    }

    array_push($breadcrumbs, get_option( 'page_on_front' ));
    return array_reverse($breadcrumbs);
}

/*
 * Defer scripts.
 **/
function theme_defer_scripts( $tag, $handle, $src ) {

    $defer_scripts = array('jquery-migrate', 'contact-form-7', 'lighbox', 'main', 'wp-embed');
    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
    }

    return $tag;
}
add_filter( 'script_loader_tag', 'theme_defer_scripts', 10, 3 );

/*
 * Preload styles.
 **/
function theme_preload_styles( $tag, $handle, $href, $media ) {

    $defer_scripts = array('style', 'contact-form-7', 'wp-block-library');
    if ( in_array( $handle, $defer_scripts ) ) {
        return '<link rel="stylesheet preload" as="style" href="' . $href . '" type="text/css" media="'. $media .'" />' . "\n";
    }

    return $tag;
}
add_filter( 'style_loader_tag', 'theme_preload_styles', 10, 4 );

/*
 * Register simple toolbar.
 **/
function simple_toolbar( $toolbars ) {

    $toolbars['Simple' ] = array();
    $toolbars['Simple' ][1] = array('bold' , 'italic' , 'underline', 'strikethrough', 'link', 'undo', 'redo', 'removeformat');

    return $toolbars;
}
add_filter( 'acf/fields/wysiwyg/toolbars', 'simple_toolbar' );


/*
 * Lazy image handling.
 **/
//function theme_lazy_alter_attachment_image($attr) {
//
//    if (isset($attr['srcset'])) unset($attr['srcset']);
//
//    if (isset($attr['class']) ) {
//        $attr['class'] = 'lazy-image';
//    }
//
//    if (isset($attr['src'])) {
//
//        $attr['data-src'] = $attr['src'];
//        unset($attr['src']);
//    }
//
//    return $attr;
//}
//add_filter('wp_get_attachment_image_attributes', 'theme_lazy_alter_attachment_image');
//
//function content_image_lazy_markup($the_content) {
//
//    libxml_use_internal_errors(true);
//    $post = new DOMDocument();
/*    $post->loadHTML('<?xml encoding="utf-8" ?>' . $the_content);*/
//    $images = $post->getElementsByTagName('img');
//
//    foreach( $images as $img ) {
//
//        if( $img->hasAttribute('data-src') ) continue;
//        if( $img->parentNode->tagName == 'noscript' ) continue;
//
//        $clone = $img->cloneNode();
//
//        $src = $img->getAttribute('src');
//        $img->removeAttribute('src');
//        $img->setAttribute('data-src', $src);
//        $img->removeAttribute('srcset');
//
//        $imgClass = $img->getAttribute('class');
//        $img->setAttribute('class', $imgClass . ' lazy-image');
//
//        $no_script = $post->createElement('noscript');
//        $no_script->appendChild($clone);
//        $img->parentNode->insertBefore($no_script, $img);
//    };
//
//    return $post->saveHTML();
//}
//add_filter('the_content', 'content_image_lazy_markup');

function theme_display_sidebar_menu() {

    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locations['primary_menu']);
    $menu_items = wp_get_nav_menu_items($menu->term_id);

    $menu_id = 0;
    $title = "";
    $has_parent = false;

    foreach ( (array) $menu_items as $key => $menu_item ) :
        if ( $menu_item->object_id == get_the_ID()) :
            $has_parent = true;
            $parent = $menu_item->menu_item_parent;
            $menu_id = $menu_item->db_id;
            $title = $menu_item->object_id;

            foreach ( (array) $menu_items as $key => $menu_item ) :
                if ( $menu_item->db_id == $parent) :
                    $parent = $menu_item->menu_item_parent;
                    $menu_id = $menu_item->db_id;
                    $title = $menu_item->object_id;

                    foreach ( (array) $menu_items as $key => $menu_item ) :
                        if ( $menu_item->db_id == $parent) :
                            $parent = $menu_item->menu_item_parent;
                            $menu_id = $menu_item->db_id;
                            $title = $menu_item->object_id;

                            foreach ( (array) $menu_items as $key => $menu_item ) :
                                if ( $menu_item->db_id == $parent) :
                                    $menu_id = $menu_item->db_id;
                                    $title = $menu_item->object_id;

                                endif;
                            endforeach;

                        endif;
                    endforeach;
                endif;
            endforeach;
        endif;
    endforeach; ?>

    <?php if ( !$has_parent ) return null;?>

    <ul class="level-1">
        <?php foreach ( (array) $menu_items as $key => $menu_item ) :
            if ($menu_item->menu_item_parent == $menu_id ) : ?>
                <li id="level-1-<?php echo $menu_item->object_id; ?>">
                    <?php $parent_id = $menu_item->db_id ?>
                    <a class="<?php echo (get_the_ID() == $menu_item->object_id) ? 'current' : ''; ?>"
                       href="<?php echo $menu_item->url ?>"><?php echo $menu_item->title ?></a>

                    <ul class="level-2">
                        <?php foreach ( (array) $menu_items as $key => $menu_item ) :
                            if ($menu_item->menu_item_parent == $parent_id ) : ?>
                                <?php $parent_id2 = $menu_item->db_id ?>
                                <li id="level-2-<?php echo $menu_item->object_id; ?>">
                                    <a class="<?php echo (get_the_ID() == $menu_item->object_id) ? 'current' : ''; ?>"
                                       href="<?php echo $menu_item->url ?>"><?php echo $menu_item->title ?></a>

                                    <ul class="level-3">
                                        <?php foreach ( (array) $menu_items as $key => $menu_item2 ) :
                                            if ($menu_item2->menu_item_parent == $parent_id2 ) : ?>
                                                <li id="level-3-<?php echo $menu_item2->object_id; ?>">
                                                    <a class="<?php echo (get_the_ID() == $menu_item2->object_id) ? 'current' : ''; ?>"
                                                       href="<?php echo $menu_item2->url ?>"><?php echo $menu_item2->title ?></a>
                                                </li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </ul>

                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>

    <?php
}

function is_woocommerce_active() {
    return class_exists( 'WooCommerce' );
}

function front_page_partial_content() { ?>

    <div class="footer-extra">
        <?php foreach ( parse_blocks( get_the_content(null, false, get_option( 'page_on_front' )) ) as $block) :
            if ($block['blockName'] == 'acf/info-block' || $block['blockName'] == 'acf/manufacturers-block') :

                echo render_block($block);

            endif;
        endforeach; ?>
    </div>

<?php
}

function front_page_partial_content_cart() { ?>

    <div class="footer-extra info-block-footer">
        <?php foreach ( parse_blocks( get_the_content(null, false, get_option( 'page_on_front' )) ) as $block) :
            if ($block['blockName'] == 'acf/info-block') :

                echo render_block($block);

            endif;
        endforeach; ?>
    </div>

    <?php
}

function google_auth_script() { ?>

  <script src="https://apis.google.com/js/api:client.js"></script>
  <script>
      let googleUser = {};
      let startApp = function() {
          gapi.load('auth2', function(){
              auth2 = gapi.auth2.init({
                client_id: '556161924206-ll373p8432nokgep6tkivbmcsd2c1hht.apps.googleusercontent.com',
                cookiepolicy: 'single_host_origin',
                // Request scopes in addition to 'profile' and 'email'
                //scope: 'additional_scope'
              });
              attachSignin(document.getElementById('googleAuth'));
        });
      };

      function attachSignin(element) {
          auth2.attachClickHandler(element, {}, function(googleUser) {

              let profile = googleUser.getBasicProfile();
              let request = new XMLHttpRequest();
              request.open('POST', '/wp-admin/admin-ajax.php', true);
              request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
              request.onload = function() {
                  if (this.status >= 200 && this.status < 400) {
                      if (this.response) {
                          location.reload();
                      }
                  }
              };
              request.send('action=TwsJNSy2Ap_authenticate_user&a=' + profile.getGivenName() + '&b=' + profile.getEmail());
              //document.getElementById('name').innerText = googleUser.getBasicProfile().getName();

          }, function(error) {
              console.log(JSON.stringify(error, undefined, 2));
          });
      }
  </script>
  <?php
}
add_action('wp_head', 'google_auth_script');

function fb_auth_script() { ?>

    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId   : '168295507958728',
                oauth   : true,
                status  : true,
                cookie  : true,
                xfbml   : true,
                version : 'v6.0'
            });
        };

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        function fb_login(){
            FB.login(function(response) {
                if (response.authResponse) {
                    //access_token = response.authResponse.accessToken;
                    //user_id = response.authResponse.userID;

                    //console.log(response);
                    FB.api('/me', function(response) {
                        let user_name = response.namel;
                        let user_email = response.email;
                        console.log(user_name + " " + user_email);
                    });

                }
            }, { scope: 'publish_actions,email'});
        }
    </script>
    <?php
}
add_action('wp_head', 'fb_auth_script');

function TwsJNSy2Ap_authenticate_user() {

    $username = $_POST['a'];
    $user_email = $_POST['b'];
    $user_id = username_exists( $username );

    if ( !$user_id && email_exists( $user_email ) == false) {

        $random_password = wp_generate_password($length = 12, $include_standard_special_chars = false);
        $user_id = wp_create_user($username, $random_password, $user_email);
    }

    if ( $user_id && email_exists( $user_email ) == true) {

        wp_set_current_user($user_id, $username);
        wp_set_auth_cookie($user_id);
    }

    echo json_encode(["authValidated" => true]);
    exit();
}
add_action('wp_ajax_TwsJNSy2Ap_authenticate_user', 'TwsJNSy2Ap_authenticate_user');
add_action('wp_ajax_nopriv_TwsJNSy2Ap_authenticate_user', 'TwsJNSy2Ap_authenticate_user');

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'wooocommerce-custom-thumb', 150, 150 );
}

// Update Price if user is shop_manager.
function add_cart_item_data( $cart_item_data, $product_id, $variation_id )
{
    $product = wc_get_product( $product_id );
    $price = $product->get_price();
    if ( is_user_logged_in() == true && wc_current_user_has_role('shop_manager') )
    {
        $whole_sale = $product->get_meta('wholesale_shop_manager_price', true);
        if ($whole_sale) {
            $cart_item_data['new_price'] = $whole_sale;
        }
    }
    return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'add_cart_item_data', 10, 3 );

add_action( 'woocommerce_before_calculate_totals', 'before_calculate_totals', 10, 1 );
function before_calculate_totals( $cart_obj )
{
    if ( is_admin() && !defined( 'DOING_AJAX' ) ) return;

    foreach ( $cart_obj->get_cart() as $key=>$value )
    {
        if( isset( $value['new_price'] ) )
        {
            $price = $value['new_price'];
            $value['data']->set_price( ( $price ) );
        }
    }
}

function product_get_whole_sale_price($product)
{
    if (is_user_logged_in())
    {
        $user_roles = wp_get_current_user()->roles;
        $main_role = '';
        $allowed_roles = array('wholesale_customer', 'shop_manager');
        $allowed = false;

        foreach ($user_roles as $role)
        {
            if (in_array($role, $allowed_roles))
            {
                $allowed = true;
                $main_role = $role;
            }
        }

        if ($allowed)
        {
            if ($main_role == 'wholesale_customer') {
                $whole_sale = $product->get_meta('wholesale_customer_wholesale_price', true);
            } else {
                $whole_sale = $product->get_meta('wholesale_shop_manager_price', true);
            }

            return $whole_sale;
        }
    }

    return null;
}

function woocommerce_product_custom_fields()
{
    $args = array(
        'id' => 'wholesale_shop_manager_price',
        'label' => __( 'Shop manager (€)', 'krona' ),
        'class' => 'wholesale_shop_manager_price',
        'desc_tip' => 'true',
        'description' => 'Only applies to users with the role of Shop manager',
    );
    woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_general_product_data', 'woocommerce_product_custom_fields' );

function woocommerce_save_custom_product_fields( $post_id )
{
    $product = wc_get_product( $post_id );

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    $wholesale_shop_manager_price = isset( $_POST['wholesale_shop_manager_price'] ) ? $_POST['wholesale_shop_manager_price'] : '';
    $product->update_meta_data( 'wholesale_shop_manager_price', sanitize_text_field( $wholesale_shop_manager_price ) );

    $product->save();
}
add_action( 'woocommerce_process_product_meta', 'woocommerce_save_custom_product_fields' );

function ajax_login()
{
    check_ajax_referer( 'ajax-login-nonce', 'security' );

    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    $info['remember'] = true;

    $user_signon = wp_signon($info, false);
    if ( is_wp_error($user_signon) ) {
        echo json_encode(array('loggedin' => false, 'message' => __('Wrong username or password.')));
    } else {
        echo json_encode(array('loggedin' => true, 'message' => __('Login successful, redirecting...')));
    }

    die();
}
add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
add_action( 'wp_ajax_ajaxlogin', 'ajax_login' );

/*
 * @action display cookie consent bar if enabled.
 **/
function gdpr_consent_bar_script()
{
    ?>
    <div id="gdpr-consent"></div>
    <?php
    if ( class_exists('acf') ) :

        $message = get_field('gdpr-text', 'option', false);
        $dismiss = get_field('gdpr-cancel', 'option');
        if ($message && $dismiss ) : ?>

            <script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js"></script>
            <script async type="text/javascript">
                window.cookieconsent.initialise({
                    container: document.getElementById("gdpr-consent"),
                    onStatusChange: function(status) {},
                    "window":
                        '<div role="dialog" aria-label=”cookieconsent" aria-describedby="cookieconsent:desc" class="cc-window">' +
                        '<span id="cookieconsent:desc" class="cc-message"><?php echo $message; ?></span>' +
                        '<div class="cc-compliance">' +
                        '<a aria-label="dismiss cookie message" role="button" tabindex="0" class="btn cc-btn cc-dismiss"><?php echo $dismiss; ?></a>' +
                        '</div>' +
                        '</div>'
                });
            </script>
        <?php
        endif;
    endif;
}
add_action( 'wp_footer', 'gdpr_consent_bar_script');

function TwsJNSy2Ap_update_cart_totals()
{
    if ( is_woocommerce_active() ) :
        global $woocommerce;
        if (wc()->cart->get_cart_contents_count() > 0) : ?>
            <span><?php echo wc()->cart->get_cart_contents_count(); ?></span>
        <?php endif; ?>
        <a href="<?php echo wc_get_cart_url(); ?>" id="cart-page-open" title="<?php _e("Pirkinių krepšelis", 'krona'); ?>">
            <h5><?php _e("Pirkinių krepšelis", 'krona'); ?></h5>
        </a>
    <?php endif;
    exit();
}
add_action('wp_ajax_TwsJNSy2Ap_update_cart_totals', 'TwsJNSy2Ap_update_cart_totals');
add_action('wp_ajax_nopriv_TwsJNSy2Ap_update_cart_totals', 'TwsJNSy2Ap_update_cart_totals');
<?php
/**
 * The template for displaying 404 pages (not found).
 */

get_header(); ?>

    <div class="post-container error-404 page-not-found">

        <div class="page-header single-header">
            <div class="container">
                <h1><?php _e('404', 'krona'); ?></h1>
            </div>
        </div>

        <div class="container inner">
            <h1><?php esc_html_e( 'Dėja, toks puslapis nerastas', 'krona' ); ?></h1>
            <p><?php esc_html_e( 'Puslapis buvo pašalintas arba perkeltas kitur.', 'krona' ); ?></p>
        </div>
    </div>

<?php
get_footer();
<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product, $post;

// Ensure visibility.
//if ( empty( $product ) || ! $product->is_visible() ) return;
?>
<li <?php wc_product_class( '', $product ); ?>>

    <span class="close-product">x</span>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	if (get_field('product-popular', get_the_ID()) == '1') : ?>
        <span class="popular"></span>
    <?php endif;

    if ( $product->is_on_sale() ) :

        if (get_field('product-sale-tag', get_the_ID()) ) : ?>
            <span class="onsale-percentage"><?php echo '-'.get_field('product-sale-tag', get_the_ID()) . '%'; ?></span>
        <?php else :
            echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale"></span>', $post, $product );
        endif;
    endif;

    $image_size = apply_filters( 'single_product_archive_thumbnail_size', 'full' );
    echo $product ? $product->get_image( $image_size ) : '';
    ?>

    <?Php if ( get_field('product-image-active', get_the_ID()) ) :

        $image_active = get_field('product-image-active', get_the_ID());
        ?>
        <img src="<?php echo $image_active['url'] ?>" class="product-image-active" alt="" />
    <?php endif; ?>

    <?php
    $whole_sale_price = product_get_whole_sale_price($product);

    if ( $price_html = $product->get_price_html() ) : ?>
        <p class="price<?php echo $whole_sale_price != null ? ' is-wholesale' : ''; ?>">
            <?php if ( $product->is_on_sale() ) : ?>
                <ins>
                    <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                        <?php
                        $price = explode(".", wc_format_decimal($product->get_sale_price(), 2));
                        echo $price[0] . ',';
                        ?>
                        <em><?php echo $price[1]; ?></em>
                    </span>
                </ins>
                <del>
                    <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                        <?php
                        $price = explode(".", wc_format_decimal($product->get_regular_price(), 2));
                        echo $price[0] . ',';
                        ?>
                        <em><?php echo $price[1]; ?></em>
                    </span>
                </del>
            <?php else : ?>
                <span class="woocommerce-Price-amount amount">
                    <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                    <?php
                    $price = explode(".", wc_format_decimal($product->get_regular_price(), 2));
                    echo $price[0] . ',';
                    ?>
                    <em><?php echo $price[1]; ?></em>
                </span>
            <?php endif; ?>
            <?php if ($whole_sale_price != null) :

                $formatted_whole_price = $whole_sale_price;
                ?>
                <span class="wholesale-price">
                    <?php _e( 'Didmeninė kaina: ', 'krona' ); ?>
                    <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                    <?php
                    $price = explode(".", wc_format_decimal($formatted_whole_price, 2));
                    echo $price[0] . ',';
                    ?>
                    <em><?php echo $price[1]; ?></em>
                </span>
            <?php endif; ?>
        </p>
    <?php endif; ?>
    <h4><?php the_title(); ?></h4>

    <div class="hidden-product-info">
        <?php wc_get_template( 'single-product/short-description.php' ); ?>
        <?php

        /**
         * Hook: woocommerce_after_shop   <p_item_title.
         *
         * @hooked woocommerce_template_loop_rating - 5
         * @hooked woocommerce_template_loop_price - 10
         */
        //do_action( 'woocommerce_after_shop_loop_item_title' );

        /**
         * Hook: woocommerce_after_shop_loop_item.
         *
         * @hooked woocommerce_template_loop_product_link_close - 5
         * @hooked woocommerce_template_loop_add_to_cart - 10
         */
        do_action( 'woocommerce_after_shop_loop_item' );
        ?>
    </div>
</li>

<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'single-product-inner', $product ); ?>>

    <?php woocommerce_show_product_images(); ?>

	<div class="summary entry-summary">
		<?php woocommerce_template_single_title(); ?>

        <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

            <span class="sku_wrapper"><?php esc_html_e( 'Prekės kodas:', 'krona' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

        <?php endif; ?>

        <div class="wrap-summary">
            <?php
            woocommerce_template_single_excerpt();
            woocommerce_template_single_add_to_cart();
            woocommerce_template_single_sharing();
            ?>
        </div>

        <div class="wrap-delivery">

            <?php
            $delivery_method = get_field('product-delivery-method', get_the_ID());
            ?>

            <div class="option option-delivery <?php echo in_array('pickup', $delivery_method) ? 'method-active' : ''; ?>">
                <img alt="<?php _e("Atsiimkite nemokamai");?>"
                     src="<?php echo get_template_directory_uri() . '/assets/images/pickup.svg'; ?>" />
                <h3><?php _e("Atsiimkite nemokamai");?></h3>
                <p><?php _e("Paruošime per 1-3 dienas"); ?></p>
            </div>

            <div class="option option-pickup <?php echo in_array('deliver', $delivery_method) ? 'method-active' : ''; ?>">
                <img alt="<?php _e("Pristatysime į namus");?>"
                     src="<?php echo get_template_directory_uri() . '/assets/images/delivery.svg'; ?>" />
                <h3><?php _e("Pristatysime į namus");?></h3>
            </div>

        </div>

        <div class="clear"></div>
        <?php

		/**
		 * Hook: woocommerce_single_product_summary.
		 *
		 * @hooked woocommerce_template_single_title - 5
		 * @hooked woocommerce_template_single_rating - 10
		 * @hooked woocommerce_template_single_price - 10
		 * @hooked woocommerce_template_single_excerpt - 20
		 * @hooked woocommerce_template_single_add_to_cart - 30
		 * @hooked woocommerce_template_single_meta - 40
		 * @hooked woocommerce_template_single_sharing - 50
		 * @hooked WC_Structured_Data::generate_product_data() - 60
		 */
		//do_action( 'woocommerce_single_product_summary' );
		?>
	</div>

    <div class="clear"></div>

	<?php if (is_single() ) :

        woocommerce_output_product_data_tabs();
        ?>

        <div class="row product-metadata">
            <div class="col-6">
                <h2 class="product-general-info"><?php _e("Bendra informacija", "storefront"); ?></h2>

                <table>
                    <tbody>
                    <?php if ( have_rows('product-general-info') ):
                        while ( have_rows('product-general-info') ) : the_row(); ?>

                            <tr>
                                <td><?php the_sub_field('product-general-info-name'); ?></td>
                                <td><?php the_sub_field('product-general-info-value'); ?></td>
                            </tr>

                        <?php endwhile;
                    endif; ?>
                    </tbody>
                </table>

            </div>
            <div class="col-6">

                <h2 class="product-properties"><?php _e("Savybės", "storefront"); ?></h2>

                <table>
                    <tbody>
                        <?php if ( have_rows('product-properties') ):
                            while ( have_rows('product-properties') ) : the_row(); ?>

                                <tr>
                                    <td><?php the_sub_field('product-properties-name'); ?></td>
                                    <td><?php the_sub_field('product-properties-value'); ?></td>
                                </tr>

                            <?php endwhile;
                        endif; ?>
                    </tbody>
                </table>

                <h2 class="product-dimensions"><?php _e("Matmenys, svoris", "storefront"); ?></h2>

                <table>
                    <tbody>
                        <?php if ( have_rows('product-dimensions') ):
                            while ( have_rows('product-dimensions') ) : the_row(); ?>

                                <tr>
                                    <td><?php the_sub_field('product-dimensions-name'); ?></td>
                                    <td><?php the_sub_field('product-dimensions-value'); ?></td>
                                </tr>

                            <?php endwhile;
                        endif; ?>
                    </tbody>
                </table>

            </div>
        </div>

    <?php
        woocommerce_upsell_display();

        // Related products
        woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args',
            array(
                'posts_per_page' => 3,
                'columns'        => 3,
                'orderby'        => 'rand',
            )
        ));
	endif; ?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
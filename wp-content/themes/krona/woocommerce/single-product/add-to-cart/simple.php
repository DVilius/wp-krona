<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<?php do_action( 'woocommerce_before_add_to_cart_quantity' ); ?>

       <p class="quantity-label"><?php _e("Kiekis", "krona"); ?></p>

        <div class="quantity-wrap">
            <button id="quantity-dec"></button>
            <?php woocommerce_quantity_input( array(
                'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
            ) ); ?>
            <button id="quantity-inc"></button>
        </div>

		<?php do_action( 'woocommerce_after_add_to_cart_quantity' ); ?>

        <?php
        $whole_sale_price = product_get_whole_sale_price($product);
        ?>

        <p class="price<?php echo $whole_sale_price != null ? ' is-wholesale' : ''; ?>">
            <?php if ( $product->is_on_sale() ) : ?>
                <ins>
                  <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                        <?php
                        $price = explode(".", wc_format_decimal($product->get_sale_price(), 2));
                        echo $price[0] . ',';
                        ?>
                        <em><?php echo $price[1]; ?></em>
                    </span>
                </ins>
                <del>
                    <span class="woocommerce-Price-amount amount">
                        <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                        <?php
                        $price = explode(".", wc_format_decimal($product->get_regular_price(), 2));
                        echo $price[0] . ',';
                        ?>
                        <em><?php echo $price[1]; ?></em>
                    </span>
                </del>
            <?php else : ?>
                <span class="woocommerce-Price-amount amount">
                    <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                    <?php
                    $price = explode(".", wc_format_decimal($product->get_regular_price(), 2));
                    echo $price[0] . ',';
                    ?>
                    <em><?php echo $price[1]; ?></em>
                </span>
            <?php endif; ?>
            <?php if ($whole_sale_price != null) :

                $formatted_whole_price = $whole_sale_price;
                ?>
                <span class="wholesale-price">
                <?php _e( 'Didmeninė kaina: ', 'krona' ); ?>
                <span class="woocommerce-Price-currencySymbol"><?php echo get_woocommerce_currency_symbol(); ?></span>
                <?php
                $price = explode(".", wc_format_decimal($formatted_whole_price, 2));
                echo $price[0] . ',';
                ?>
                <em><?php echo $price[1]; ?></em>
            </span>
            <?php endif; ?>
        </p>

		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php _e('Idėti į krepšelį') ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>

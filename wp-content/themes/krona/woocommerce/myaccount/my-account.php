<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
//do_action( 'woocommerce_account_navigation' ); ?>

<?php
global $wp;
$account_page = '';
if ( ! empty( $wp->query_vars ) ) {
    foreach ($wp->query_vars as $key => $value) {
        if ('pagename' === $key) continue;
        $account_page = $key;
    }
}
?>

<div class="woocommerce-MyAccount-content">

    <?php if ($account_page == 'page') : ?>

        <ul class="woocommerce-account-navigation-links">
            <li>
                <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>edit-account">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/account-user.svg' ?>"
                         alt="<?php _e('Informacija', 'krona'); ?>"/>
                    <?php _e('Informacija', 'krona'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>edit-address">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/account-marker.svg' ?>"
                         alt="<?php _e('Adresai', 'krona'); ?>"/>
                    <?php _e('Adresai', 'krona'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>orders">
                    <img src="<?php echo get_template_directory_uri() . '/assets/images/account-orders.svg' ?>"
                         alt="<?php _e('Užsakymų istorija', 'krona'); ?>"/>
                    <?php _e('Užsakymų istorija', 'krona'); ?>
                </a>
            </li>
        </ul>

    <?php else : ?>

	<?php
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */
		do_action( 'woocommerce_account_content' );
	?>

    <?php endif; ?>

    <?php if ($account_page == 'page') : ?>
        <a class="account-logout" href="<?php echo esc_url( wc_get_account_endpoint_url( get_option( 'woocommerce_logout_endpoint', 'customer-logout' ) ) ); ?>">
            <?php _e('Atsijungti', 'krona'); ?>
        </a>
    <?php endif; ?>

</div>

<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

defined( 'ABSPATH' ) || exit;

$customer_id = get_current_user_id();

if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		array(
			'billing'  => __( 'Billing address', 'woocommerce' ),
			'shipping' => __( 'Shipping address', 'woocommerce' ),
		),
		$customer_id
	);
} else {
	$get_addresses = apply_filters(
		'woocommerce_my_account_get_addresses',
		array(
			'billing' => __( 'Billing address', 'woocommerce' ),
		),
		$customer_id
	);
}
?>

<!--<p>-->
<!--	--><?php //echo apply_filters( 'woocommerce_my_account_my_address_description', esc_html__( 'The following addresses will be used on the checkout page by default.', 'woocommerce' ) ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
<!--</p>-->

    <div class="woocommerce-address-fields account-address-manage">
        <form method="post">
            <?php
            $checkout = new WC_Checkout();
            $fields = $checkout->get_checkout_fields( 'billing');

            $customer_info_name = array('billing_first_name', 'billing_last_name');
            $customer_info_fields = array('billing_company', 'company_code');
            $customer_address_fields = array('billing_email', 'billing_state', 'billing_country', 'billing_city', 'billing_address_1', 'billing_postcode');

            foreach ($fields as $key => $field) {
                if (in_array($key, $customer_info_name )) {
                    $field['return'] = true;
                    echo woocommerce_form_field($key, $field, $checkout->get_value($key));
                }
            }
            echo '<div class="clear"></div>';

            foreach ($fields as $key => $field) {
                if (in_array($key, $customer_info_fields)) {
                    $field['return'] = true;
                    echo woocommerce_form_field($key, $field, $checkout->get_value($key));
                }
            }

            echo '<div class="clear"></div>';

            foreach ($fields as $key => $field) {
                if (in_array($key, $customer_address_fields)) {
                    $field['return'] = true;
                    $field['class'] = array('form-row-wide');
                    echo woocommerce_form_field($key, $field, $checkout->get_value($key));
                }
            } ?>

            <button type="submit" class="btn button account-save-address" name="save_address"
                    value="<?php esc_attr_e( 'Išsaugoti', 'krona' ); ?>">
                <?php esc_html_e( 'Išsaugoti', 'krona' ); ?>
            </button>
            <?php wp_nonce_field( 'woocommerce-edit_address', 'woocommerce-edit-address-nonce' ); ?>
            <input type="hidden" name="action" value="edit_address" />
        </form>
    </div>

<?php
//foreach ( $get_addresses as $name => $address_title ) : ?>
<!--	--><?php
//		$address = wc_get_account_formatted_address( $name );
//	?>
<!---->
<!--	<div class="u-column woocommerce-Address">-->
<!--		<header class="woocommerce-Address-title title">-->
<!--			<a href="--><?php //echo esc_url( wc_get_endpoint_url( 'edit-address', $name ) ); ?><!--" class="edit">--><?php //echo $address ? esc_html__( 'Edit', 'woocommerce' ) : esc_html__( 'Add', 'woocommerce' ); ?><!--</a>-->
<!--		</header>-->
<!--		<address>-->
<!--			--><?php
//				echo $address ? wp_kses_post( $address ) : esc_html_e( 'You have not set up this type of address yet.', 'woocommerce' );
//			?>
<!--		</address>-->
<!--	</div>-->
<!---->
<?php //endforeach; ?>
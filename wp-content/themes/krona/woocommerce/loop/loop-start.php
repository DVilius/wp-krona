<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<?php
$queried_object = get_queried_object();
if ( !is_single() ) : ?>

<div class="woocommerce-product-categories woocommerce-filter-column">

<!--    --><?php //if ($queried_object->taxonomy == null || $queried_object->parent == 0) : ?>
<!---->
<!--        <ul class="sub-menu">-->
<!--            --><?php
//            $parent = 0;
//            $product_categories = get_terms(array(
//                'taxonomy'   => "product_cat",
//                'orderby'    => "menu_order",
//                'order'      => "ASC",
//                'hide_empty' => false,
//                'exclude'    => array(15),
//            ));
//            foreach ($product_categories as $cat) :
//
//                if ($cat->parent == 0 && $queried_object->term_id == $cat->term_id
//                || $cat->parent == 0 && $queried_object->term_id == null) :
//                    $parent = $cat->term_id;
//                    $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
//                    $image = wp_get_attachment_url( $thumbnail_id );
//                    ?>
<!--                    <li class="product-cat---><?php //echo $cat->slug ?><!--">-->
<!--                        <a href="--><?php //echo get_term_link($cat->term_id) ?><!--">-->
<!--                            <img src="--><?php //echo $image?><!--" alt="--><?php //echo $cat->name ?><!--"/>-->
<!--                            --><?php //echo $cat->name ?>
<!--                        </a>-->
<!--                    </li>-->
<!--                    <ul class="sub-menu">-->
<!--                        --><?php
//                        foreach ($product_categories as $cat_2) :
//                            if ($cat_2->parent == $parent) : ?>
<!--                                <li class="product-cat---><?php //echo $cat_2->slug ?><!--">-->
<!--                                    <a href="--><?php //echo get_term_link($cat_2->term_id) ?><!--">--><?php //echo $cat_2->name ?><!--</a>-->
<!--                                </li>-->
<!--                            --><?php //endif; ?>
<!--                        --><?php //endforeach; ?>
<!--                    </ul>-->
<!--                --><?php //endif; ?>
<!--            --><?php //endforeach; ?>
<!--        </ul>-->
<!---->
<!--    --><?php //endif; ?>

    <?php if ($queried_object->taxonomy != null && $queried_object->parent != 0) : ?>

        <?php
        $pricing_range = theme_woocommerce_product_pricing_range(get_queried_object()->term_id);
        $cost_range_min = $pricing_range[0];
        $cost_range_max = $pricing_range[1];
        ?>

        <script>
            jQuery(document).ready(function( $ ) {

                let update_timeout;
                let costRange = $( "#cost-range" );
                let range_min = $( "#cost-range-min" );
                let range_max = $( "#cost-range-max" );

                costRange.slider({
                    range: true,
                    min: <?php echo $cost_range_min; ?>,
                    max: <?php echo $cost_range_max; ?>,
                    values: [
                        <?php echo isset($_GET["crs"]) && $_GET["crs"] != '' ? $_GET["crs"] : $cost_range_min; ?>,
                        <?php echo isset($_GET["cre"]) && $_GET["cre"] != '' ? $_GET["cre"] : $cost_range_max; ?>
                    ],

                    slide: function( event, ui ) {
                        $("#cost-range-min").val(ui.values[0]);
                        $("#cost-range-max").val(ui.values[1]);

                        $('.cost-range-span').remove();
                        $('#active-product-filters')
                            .append('<span class="cost-range-span" id="cost-range-span">' +
                                ui.values[0] + ' - ' + ui.values[1] +
                                ' <?php echo get_woocommerce_currency_symbol(); ?>' +
                                '</span>');

                        history.replaceState(null, null, replaceUrlParam(window.location.href, 'crs', ui.values[0]));
                        history.replaceState(null, null, replaceUrlParam(window.location.href, 'cre', ui.values[1]));

                        if (update_timeout) clearTimeout(update_timeout);
                        update_timeout = setTimeout(location.reload(), 3000);
                    }
                });

                range_min.val( costRange.slider( "values", 0 ) );
                range_max.val( costRange.slider( "values", 1 ) );

                $('#active-product-filters')
                    .append('<span class="cost-range-span" id="cost-range-span">' +
                        range_min.val() + ' - ' + range_max.val() +
                        ' <?php echo get_woocommerce_currency_symbol(); ?>' +
                        '</span>');

                range_min.change(function() {
                    costRange.slider( "values", 0, $(this).val());
                });

                range_max.change(function() {
                    costRange.slider( "values", 1, $(this).val());
                });

                $('#active-product-filters').on( "click", 'span', function() {

                    if ( $(this).hasClass('cost-range-span')) {

                        history.replaceState(null, null, replaceUrlParam(window.location.href, 'crs', ''));
                        history.replaceState(null, null, replaceUrlParam(window.location.href, 'cre', ''));

                        if (update_timeout) clearTimeout(update_timeout);
                        update_timeout = setTimeout(location.reload(), 1500);

                        $(this).remove();
                        return;
                    }

                    let attribute = $('.filter-single-wrap').find('input[value='+ $(this).attr("id") +']');
                    attribute.prop( "checked", false );

                    let filter_name = "pa_" + attribute.val().split("_")[1];
                    let filter_values = [];
                    $('.filter-single-' + filter_name + " input").each(function( key, value ) {
                        if ( this.checked ) filter_values.push($(this).val().split("_")[2]);
                    });

                    history.replaceState(null, null,
                        replaceUrlParam(window.location.href, filter_name.split("_")[1], filter_values.toString())
                    );

                    $(this).remove();

                    if (update_timeout) clearTimeout(update_timeout);
                    update_timeout = setTimeout(location.reload(), 1500);
                });

                $('.product-clear-filters').on('click', function()
                {
                    history.replaceState(null, null, window.location.href.split('?')[0]);
                    if (update_timeout) clearTimeout(update_timeout);
                    update_timeout = setTimeout(location.reload(), 500);
                });

                $('.filter-single-wrap input').change(function()
                {
                    if (this.checked) {
                        $('#active-product-filters')
                            .append('<span id="'+ $(this).val() +'">' + $(this).attr("name") + '</span>');
                    } else {
                        $('#active-product-filters')
                            .find("#" + $(this).val()).remove();
                    }

                    let filter_name = $(this).val().replace( "_" + $(this).val().split("_")[2], "");
                    let filter_values = [];
                    $('.filter-single-' + filter_name + " input").each(function( key, value ) {
                        if ( this.checked ) filter_values.push($(this).val().split("_")[2]);
                    });

                    history.replaceState(null, null,
                        replaceUrlParam(window.location.href, $(this).val().split("_")[1], filter_values.toString())
                    );

                    if (update_timeout) clearTimeout(update_timeout);
                    update_timeout = setTimeout(location.reload(), 1500);
                });

                // Append active filters to filter list.
                $('.filter-single-wrap').each(function() {
                    $(this).find("input:checkbox:checked").each(function() {

                        let value = $(this).val();
                        let shown = false;
                        $('#active-product-filters span').each(function() {

                            if ( $(this).attr("id") === value ) {
                                shown = true;
                            }
                        });

                        if ( !shown ) {
                            $('#active-product-filters')
                                .append('<span id="'+ $(this).val() +'">' + $(this).attr("name") + '</span>')
                        }
                    });
                });

                function replaceUrlParam(url, paramName, paramValue)
                {
                    if (paramValue == null) paramValue = '';

                    var pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');
                    if (url.search(pattern) >= 0) {

                        url = url.replace(pattern, '$1' + paramValue + '$2');

                        if (paramValue === '') {
                            url = url.replace(paramName + "=", '');
                        }

                        return url;
                    }
                    url = url.replace(/[?#]$/,'');
                    url = url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;

                    if (paramValue === '') {
                        url = url.replace(paramName + "=", '');
                    }

                    return url;
                }

            });
        </script>

        <div class="filter-active-wrap">
            <h4><?php _e("Aktyvus filtrai", "krona");?></h4>
            <div id="active-product-filters"></div>
            <button class="product-clear-filters"><?php _e("Išvalyti visus", "krona");?></button>
        </div>

        <div class="filter-wrap">

            <h4><?php _e("Kaina", "storefront"); ?></h4>
            <div class="cost-range-wrap">
                <label>
                    <?php echo get_woocommerce_currency_symbol(); ?>
                    <input type="number" id="cost-range-min">
                </label>
                <label>
                    <?php echo get_woocommerce_currency_symbol(); ?>
                    <input type="number" id="cost-range-max">
                </label>
            </div>
            <div id="cost-range"></div>

            <?php
            $attributes_shown = array();
            $attributes_data_shown = array();
            $attributes_list = array();

            foreach ( wc_get_products(array('category'  => get_queried_object()->slug, 'limit' => -1)) as $product ) :
                if (count($product->get_attributes()) > 0) {
                    foreach ( $product->get_attributes() as $attr_name => $attr ) :
                        foreach ( $attr->get_terms() as $term ) :
                            array_push($attributes_list, $term);
                        endforeach;
                    endforeach;
                }
            endforeach;

            foreach ( wc_get_products(array('category' => get_queried_object()->slug, 'limit' => -1)) as $product ) :
                foreach ( $product->get_attributes() as $attr_name => $attr ) :

                    if ( !in_array($attr_name, $attributes_shown) ) :
                        array_push($attributes_shown, $attr_name);
                        ?>

                        <h4 class="filter-attribute-name">
                            <?php echo wc_attribute_label( $attr_name ); ?>
                        </h4>
                        <ul class="filter-single-wrap filter-single-<?php echo $attr_name; ?>">
                            <?php foreach ( $attributes_list as $term ) :

                                $attribute = $_GET[explode("pa_", $attr_name)[1]];
                                if ( $term->taxonomy != $attr_name ) continue;

                                if ( in_array($term->term_id, $attributes_data_shown) ) continue;
                                array_push($attributes_data_shown, $term->term_id);
                                ?>
                                <li>
                                    <label>
                                        <input type="checkbox"
                                               name="<?php echo $term->name; ?>"
                                               value="<?php echo $attr_name . '_' . $term->term_id; ?>"
                                            <?php echo isset($attribute) && in_array($term->term_id, explode(",", $attribute)) ? 'checked' : ''?>
                                        />
                                        <?php echo $term->name; ?>

                                        <?php
                                        $attribute_query = new WP_Query(array(
                                            'post_type'      => 'product',
                                            'post_status'    => 'publish',
                                            'posts_per_page' => -1,
                                            'tax_query' => array(
                                                array (
                                                    'taxonomy' => $term->taxonomy,
                                                    'field' => 'slug',
                                                    'terms' => $term->slug,
                                                ),
                                                array (
                                                    'taxonomy' => 'product_cat',
                                                    'field' => 'slug',
                                                    'terms' => $queried_object->slug,
                                                )
                                            ),
                                        ));
                                        $count = 0;
                                        while ( $attribute_query->have_posts() ) : $attribute_query->the_post();
                                            $count++;
                                        endwhile; ?>

                                        <span>(<?php echo $count; ?>)</span>
                                    </label>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>

        </div>
    <?php endif; ?>
    </div>

   <?php if ($queried_object->taxonomy != null && $queried_object->parent != 0) : ?>
    <div class="filter-collapse" id="filter-collapse"><?php _e("Filtras", 'krona'); ?></div>
    <?php endif; ?>
<?php endif; ?>

<div class="woocommerce-product-column">

    <?php if ( !is_single() ) : ?>
        <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
            <h1 class="h2 woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
        <?php endif; ?>

        <?php
        $queried_object = get_queried_object();
        if ($queried_object->parent != 0) : ?>

        <div class="woocommerce-category-meta-wrap">
            <?php
            $product_query = new WP_Query(theme_product_archive_query_args());
            $found_posts = $product_query->found_posts;

            $page = isset( $current ) ? $current : wc_get_loop_prop( 'current_page' );
            $from_product = $page > 1 ? (24 * $page) - 24 : 1;
            $to_product = $page > 1 ? $from_product + $product_query->post_count : 24;
            $to_product = $to_product > $found_posts ? $found_posts : $to_product;
            ?>

            <div class="category-products-count">
                <span><?php _e('Rodoma', 'storefront'); ?></span>
                    <?php echo $from_product . '–' . $to_product; ?>
                <span><?php _e('iš', 'storefront'); ?></span>
                <?php echo $found_posts; ?>
            </div>

            <div class="category-product-sort">
                <span><?php _e('Rūšiuoti pagal:', 'storefront'); ?></span>
                <?php woocommerce_catalog_ordering(); ?>
            </div>
        </div>
        <?php endif; ?>
    <?php endif; ?>

<ul class="products">

<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>

<!--[if IE]>
<style>
    input[type='radio'] {
        height: 10px;
        width: 10px;
    }
</style>
<![endif]-->

<div class="woocommerce-billing-fields">

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div class="woocommerce-billing-fields__field-wrapper">
        <?php
        $fields = $checkout->get_checkout_fields( 'billing' );
        $customer_info_fields = array('billing_first_name', 'billing_last_name', 'billing_phone', 'billing_company', 'company_code', 'billing_email');
        $customer_delivery_fields = array('delivery_type');
        ?>

        <div class="form-billing-tab-wrap customer-info-wrap">

            <div class="form-billing-tab customer-info-tab">
                <h2><?php _e('1. Asmeninė informacija', 'storefront'); ?></h2>
            </div>

            <div class="woocommerce-form-login-toggle">
                <?php if (!is_user_logged_in()) : ?>
                    <div class="radio-select">
                        <input type="radio"
                               id="checkout-guest"
                               name="customer-checkout-method"
                               value="checkout-guest" checked/>
                        <label for="checkout-guest"><?php _e('Pirkti kaip svečiui', 'storefront'); ?></label>
                    </div>
                    <div class="radio-select">
                        <input type="radio"
                               id="checkout-customer"
                               name="customer-checkout-method"
                               value="checkout-customer" />
                        <label for="checkout-customer"><?php _e('Prisijungti', 'storefront'); ?></label>
                    </div>
                <?php endif; ?>
            </div>

            <div class="checkout-login-fields">

                <p class="form-user form-user-username">
                    <label for="username"><?php esc_html_e( 'Username or email', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input type="text" class="input-text" name="username" id="username" autocomplete="username" />
                </p>
                <p class="form-user form-user-password">
                    <label for="password"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                    <input class="input-text" type="password" name="password" id="password" autocomplete="current-password" />
                </p>

                <p class="form-user">
                    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
                        <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
                    </label>
                    <button type="button" data-action="<?php echo admin_url( 'admin-ajax.php' ); ?>"
                            class="woocommerce-button button ajax-loginuser"
                            value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>">
                        <?php esc_html_e( 'Login', 'woocommerce' ); ?>
                    </button>
                    <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                </p>

                <p class="lost_password form-user">
                    <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
                </p>

            </div>

            <div>
                <?php //woocommerce_checkout_login_form(); ?>
            </div>

            <?php if ( !is_user_logged_in() && $checkout->is_registration_enabled() ) : ?>
                <div class="woocommerce-account-fields">

                    <?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

                    <?php if ( $checkout->get_checkout_fields( 'account' ) ) : ?>

                        <?php foreach ( $checkout->get_checkout_fields( 'account' ) as $key => $field ) :
                            $field['return'] = true;
                            echo woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
                        endforeach; ?>

                    <?php endif; ?>

                    <?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
                </div>
            <?php endif; ?>

            <div class="form-billing-tab-content customer-info-fields">
                <div>
                    <?php foreach ( $fields as $key => $field ) {
                        if (in_array($key, $customer_info_fields)) {
                            $field['return'] = true;
                            echo woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
                        }
                    } ?>
                </div>

                <?php if ( apply_filters( 'woocommerce_checkout_show_terms', true )
                    && function_exists( 'wc_terms_and_conditions_checkbox_enabled' ) ) : ?>

                    <div class="woocommerce-terms-and-conditions-wrapper">
                        <?php
                        do_action( 'woocommerce_checkout_before_terms_and_conditions' );

                        /**
                         * Terms and conditions hook used to inject content.
                         *
                         * @since 3.4.0.
                         * @hooked wc_checkout_privacy_policy_text() Shows custom privacy policy text. Priority 20.
                         * @hooked wc_terms_and_conditions_page_content() Shows t&c page content. Priority 30.
                         */
                        do_action( 'woocommerce_checkout_terms_and_conditions' );
                        ?>

                        <?php if ( wc_terms_and_conditions_checkbox_enabled() ) : ?>
                            <p class="form-row validate-required">
                                <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                                    <input type="checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" name="terms" <?php checked( apply_filters( 'woocommerce_terms_is_checked_default', isset( $_POST['terms'] ) ), true ); // WPCS: input var ok, csrf ok. ?> id="terms" />
                                    <span class="woocommerce-terms-and-conditions-checkbox-text"><?php wc_terms_and_conditions_checkbox_text(); ?></span>&nbsp;<span class="required">*</span>
                                </label>
                                <input type="hidden" name="terms-field" value="1" />
                            </p>
                        <?php endif; ?>

                        <?php do_action( 'woocommerce_checkout_after_terms_and_conditions' ); ?>
                    </div>
                <?php endif; ?>

                <button id="form-continue-address"><?php _e('Tęsti', 'storefront'); ?></button>

            </div>

        </div>

	</div>

    <div class="form-billing-tab-wrap customer-delivery-wrap">
        <div class="form-billing-tab customer-delivery-tab">
            <h2><?php _e('2. Adresai', 'storefront'); ?></h2>
        </div>
        <div class="form-billing-tab-content customer-delivery tab-closed">
            <div>
                <?php foreach ( $fields as $key => $field ) {
                    if ( !in_array($key, $customer_info_fields) && !in_array($key, $customer_delivery_fields) ) {
                        $field['return'] = true;
                        echo woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
                    }
                } ?>
            </div>

            <script src="https://code.jquery.com/jquery-3.4.1.min.js"
                    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
                    crossorigin="anonymous"></script>
            <script>
                jQuery( document.body ).trigger( 'wc_fragment_refresh' );
                jQuery('#billing_country').select2();
            </script>

            <button id="form-continue-delivery"><?php _e('Tęsti', 'storefront'); ?></button>
        </div>
    </div>

    <div class="form-billing-tab-wrap delivery-method-wrap">
        <div class="form-billing-tab delivery-method-tab">
            <h2><?php _e('3. Pristatymo būdas', 'storefront'); ?></h2>
        </div>
        <div class="form-billing-tab-content delivery-method tab-closed">

            <?php do_action("theme_checkout_shipping_data"); ?>

<!--            <div>-->
<!--                --><?php //foreach ( $fields as $key => $field ) {
//                    if ( in_array($key, $customer_delivery_fields) ) {
//                        $field['return'] = true;
//                        echo woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
//                    }
//                } ?>
<!--            </div>-->

            <button id="form-continue-payment"><?php _e('Tęsti', 'storefront'); ?></button>

        </div>

    </div>

    <div class="form-billing-tab-wrap payment-method-wrap">
        <div class="form-billing-tab payment-method-tab">
            <h2><?php _e('4. Mokėjimas', 'storefront'); ?></h2>
        </div>
        <div class="form-billing-tab-content payment-method tab-closed">
            <div><?php woocommerce_checkout_payment(); ?></div>
        </div>

    </div>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>

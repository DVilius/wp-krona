<?php
/**
 * The template for displaying contacts.
 * Template Name: Contacts
 */
?>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
            integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
            crossorigin=""></script>


<?php get_header(); ?>

	<div class="page-wrap wrap contact-page">
        <ul class="breadcrumbs">
            <?php foreach (get_theme_breadcrumbs() as $id) : ?>
                <li>
                    <a href="<?php echo get_the_permalink($id) ?>"><?php echo get_the_title($id); ?></a>
                    <span></span>
                </li>
            <?php endforeach; ?>
        </ul>

        <h1><?php the_title(); ?></h1>

        <div class="row">
            <div class="column-1">
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="post-content">
                        <?php the_content(); ?>
                    </div>
                <?php endwhile; ?>
            </div>

            <div class="column-2">
                <?php echo do_shortcode('[contact-form-7 id="1187" title="Kontaktai forma"]'); ?>
            </div>

            <div class="column-full">
                <div class="map" id="map" style="height: 500px;"></div>
                <script>
                    let map = L.map('map', {
                        scrollWheelZoom: false
                    }).setView([55.7034034,21.2060067], 13);
                    // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    //     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    // }).addTo(map);
                    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZGVtb3ZpbGl1cyIsImEiOiJjam5zeTJsaWswM2UxM3JvMXppamxkeXl2In0.gSBxj7rBcrJANrH_IzleiA', {
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                        maxZoom: 18,
                        id: 'demovilius/ck7bolncg02ex1io7dbc25hr2',
                        tileSize: 512,
                        zoomOffset: -1,
                    }).addTo(map);
                    let icon = L.icon({
                        iconUrl: '<?php echo get_template_directory_uri() . '/assets/images/marker.svg'; ?>',
                        iconSize:     [180, 180],
                        popupAnchor:  [-3, -76]
                    });
                    L.marker([55.7034034,21.2060067], {icon: icon}).addTo(map);
                </script>
            </div>

        </div>

	</div>

    <?php front_page_partial_content(); ?>

<?php get_footer(); ?>
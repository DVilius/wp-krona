<?php
/**
 * The template for displaying search results.
 */

get_header(); ?>

	<div class="page-wrap wrap">
		<?php
        $match = array();
        $search_query = new WP_Query(array(
            's' => $_GET['s'],
            'orderby' => 'name',
            'order'   => 'DESC',
            'post_type' => 'product',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
        ));

        if ( $search_query->have_posts() ) {
            while ( $search_query->have_posts() ) : $search_query->the_post();
                array_push($match, get_the_ID());
            endwhile;
        }

        $tag_query = new WP_Query(array(
            'post_status'    => 'publish',
            'post_type' => 'product',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'product_tag',
                    'field' => 'slug',
                    'terms' => explode(' ', $_GET['s']),
                )
            )
        ));

        if ( $tag_query->have_posts() ) {
            while ( $tag_query->have_posts() ) : $tag_query->the_post();
                array_push($match, get_the_ID());
            endwhile;
        }

        $paged = $_GET['pg'] ? $_GET['pg'] : 1;
        $query = new WP_Query(array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'post__in' => $match,
            'posts_per_page' => 12,
            'paged' => $paged,
        ));

        if ( count($match) > 0 && $query->have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php printf(esc_attr__( 'Paieškos rezultatai pagal raktažodį: %s', 'krona' ),
                        '<span>' . get_search_query() . '</span>'
                    ); ?>
				</h1>
			</header>

            <div class="woocommerce-product-column" style="width: 100%; float: none; padding: 0">
                <ul class="products">
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                        <?php wc_get_template_part('content', 'product'); ?>

                    <?php endwhile; ?>
                </ul>
            </div>

            <div class="woocommerce-pagination">
                <?php
                $big = 999999999; // need an unlikely integer
                $total_pages = $query->max_num_pages;
                echo paginate_links( array(
                    'base' => '%_%',
                    'format' => '?pg=%#%',
                    'current'    => max( 1, $paged ),
                    'total'      => $total_pages,
                    'next_text' => null,
                    'prev_text' => null,
                ) );
                ?>
            </div>

		<?php else :
			get_template_part( 'content', 'none' );
		endif;
		?>
	</div>

<?php
get_footer();
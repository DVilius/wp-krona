<?php
/**
* The template for product sales
* Template Name: Sales
*/
?>

<?php get_header(); ?>

    <div class="front-content">
        <div class="page-wrap wrap">
            <ul class="breadcrumbs">
                <?php foreach (get_theme_breadcrumbs() as $id) : ?>
                    <li>
                        <a href="<?php echo get_the_permalink($id) ?>"><?php echo get_the_title($id); ?></a>
                        <span></span>
                    </li>
                <?php endforeach; ?>
            </ul>

            <div class="product-sale-block">
                <h1><?php the_title(); ?></h1>
                <ul class="products product-list-wrap">
                    <?php
                    $query = new WP_Query(array(
                        'post_type'      => 'product',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'meta_query'     => array(
                            'relation' => 'OR',
                            array (
                                'key'           => '_sale_price',
                                'value'         => 0,
                                'compare'       => '>',
                                'type'          => 'numeric'
                            ),
                            array (
                                'key'           => '_min_variation_sale_price',
                                'value'         => 0,
                                'compare'       => '>',
                                'type'          => 'numeric'
                            )
                        )
                    ));

                    if ( $query->have_posts() ) : ?>
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                            <?php wc_get_template_part( 'content', 'product' ); ?>

                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>

<?php front_page_partial_content();  ?>

<?php get_footer(); ?>
<?php
/**
 * The template for displaying archive pages.
 */

get_header(); ?>

	<div class="container">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->

            <?php while ( have_posts() ) : the_post(); ?>

                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>

            <?php endwhile; ?>

		<?php else :

			get_template_part( 'content', 'none' );

		endif; ?>

	</div>

<?php
get_footer();

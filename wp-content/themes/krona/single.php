<?php
/**
 * The template for displaying all single posts.
 */

get_header(); ?>

	<div class="wrap">
        <?php while ( have_posts() ) : the_post(); ?>

            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>

        <?php endwhile; ?>
	</div>

<?php
get_footer();
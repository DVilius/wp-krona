<?php

/*
 * Register gutenberg blocks.
 * */
function theme_acf_register_gutenberg_blocks() {
    if ( function_exists('acf_register_block') ) {
        acf_register_block(array(
            'name'				=> 'product-carousel',
            'title'				=> __('Product Carousel', 'krona'),
            'description'		=> __('Product Carousel.', 'krona'),
            'render_template'   => 'template-parts/gutenberg-blocks/product-carousel.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'carousel', 'slide' ),
        ));
        acf_register_block(array(
            'name'				=> 'product-sale',
            'title'				=> __('Product Sale List', 'krona'),
            'description'		=> __('Product Sale List.', 'krona'),
            'render_template'   => 'template-parts/gutenberg-blocks/product-sale.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'product', 'sale' ),
        ));
        acf_register_block(array(
            'name'				=> 'product-category-block',
            'title'				=> __('Product Category block', 'krona'),
            'description'		=> __('Product Category block.', 'krona'),
            'render_template'   => 'template-parts/gutenberg-blocks/product-category-block.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'product', 'category' ),
        ));
        acf_register_block(array(
            'name'				=> 'info-block',
            'title'				=> __('Info block', 'krona'),
            'description'		=> __('Info block.', 'krona'),
            'render_template'   => 'template-parts/gutenberg-blocks/info-blocks.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'info' ),
        ));
        acf_register_block(array(
            'name'				=> 'manufacturers-block',
            'title'				=> __('Manufacturers', 'krona'),
            'description'		=> __('Manufacturers block.', 'krona'),
            'render_template'   => 'template-parts/gutenberg-blocks/manufacturers.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'manufacturers' ),
        ));
        acf_register_block(array(
            'name'				=> 'qna-block',
            'title'				=> __('DUK', 'krona'),
            'description'		=> __('Dažnai užduodami klausimai.', 'krona'),
            'render_template'   => 'template-parts/gutenberg-blocks/qna.php',
            'category'			=> 'formatting',
            'icon'				=> 'admin-comments',
            'keywords'			=> array( 'qna' ),
        ));
    }
}
add_action('acf/init', 'theme_acf_register_gutenberg_blocks');

/*
 * Enqueue gutenberg editor styles
 * */
function theme_acf_admin_enqueue_styles() {
    wp_register_style( 'gutenberg-editor', get_stylesheet_directory_uri() . '/editor.css', false, '1.0.0' );
    wp_enqueue_style( 'gutenberg-editor' );
}
add_action( 'acf/input/admin_enqueue_scripts', 'theme_acf_admin_enqueue_styles' );
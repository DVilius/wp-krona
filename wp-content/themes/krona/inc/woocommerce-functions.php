<?php
/*
 * WooCommerce functions.
 * */

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 4 );

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10);

/*
 * Unhook default styling.
 * */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

function theme_woocommerce_change_breadcrumbs( $defaults ) {
    $defaults['delimiter'] = ' ';
    $defaults['home'] = __( 'Titulinis', 'breadcrumb', 'krona' );
    return $defaults;
}
add_filter( 'woocommerce_breadcrumb_defaults', 'theme_woocommerce_change_breadcrumbs' );


function theme_woocommerce_product_pricing_range($category_id) {

    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => -1,
        'orderby'        => 'meta_value_num',
        'meta_key'       => '_price',
        'order'          => 'DESC'
    );

    if ($category_id) {
        $args['tax_query'] = array(array(
            'taxonomy'        => 'product_cat',
            'field'           => 'term_id',
            'terms'           => $category_id,
        ));
    }

    $products = get_posts($args);

    if ( !$products ) {
        $cost_range_max = 0;
        $cost_range_min = 0;
    } else {
        $cost_range_max = wc_get_product($products[0]->ID)->get_price();
        $cost_range_min = wc_get_product($products[ count($products) - 1 ]->ID)->get_price();
    }

    return array($cost_range_min, $cost_range_max);
}

function theme_product_archive_query_args() {

    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $product_args = array(
        'post_type'      => 'product',
        'paged'          => $paged,
        'post_status'    => 'publish',
        'posts_per_page' => 24,
        'tax_query'      => array(),
    );

    if (get_queried_object() && get_queried_object()->term_id ) {
        array_push($product_args['tax_query'], array(
            'taxonomy'        => 'product_cat',
            'field'           => 'term_id',
            'terms'           => get_queried_object()->term_id,
        ));
    }

    if ( isset($_GET['orderby']) ) {

        if ($_GET['orderby'] === "date") {
            $product_args['orderby'] = $_GET['orderby'];
            $product_args['order'] = 'DESC';
        }

        if ($_GET['orderby'] === "popularity") {
            $product_args['meta_key'] = 'total_sales';
            $product_args['orderby'] = 'meta_value_num';
            $product_args['order'] = 'ASC';
        }

        if ($_GET['orderby'] === "price") {

            $product_args['meta_key'] = '_price';
            $product_args['orderby'] = 'meta_value_num';
            $product_args['order'] = 'ASC';
        }

        if ($_GET['orderby'] === "price-desc") {
            $product_args['meta_key'] = '_price';
            $product_args['orderby'] = 'meta_value_num';
            $product_args['order'] = 'DESC';
        }

    }

    $args = array(
        'category'  => get_queried_object()->slug
    );

    foreach ( wc_get_products($args) as $pro ) :
        foreach ( $pro->get_attributes() as $attr_name => $attr ) :

            $attribute_name = explode("pa_", $attr_name)[1];
            $attribute_values = $_GET[$attribute_name];

            if ( isset($attribute_values) && $attribute_values != '' ) {
                array_push($product_args['tax_query'], array(
                    'taxonomy'        => $attr_name,
                    'field'           => 'term_id',
                    'terms'           => explode(',', $attribute_values),
                    'operator'        => 'IN',
                ));
            }

        endforeach;
    endforeach;

    if ( isset($_GET['crs']) && isset($_GET['cre']) ) {
        $meta_query = array(
            array(
                'key' => '_price',
                'value' => array($_GET['crs'], $_GET['cre']),
                'compare' => 'BETWEEN',
                'type' => 'NUMERIC'
            )
        );
        $product_args['meta_query'] = $meta_query;
    }

    return $product_args;
}

/**
 * Re-prioritize WooCommerce checkout fields.
 */

function custom_override_checkout_fields($fields)
{
    $fields['billing']['billing_first_name']['priority'] = 1;
    $fields['billing']['billing_first_name']['class'] = array('form-row-first');

    $fields['billing']['billing_last_name']['priority'] = 2;
    $fields['billing']['billing_last_name']['class'] = array('form-row-last');

    $fields['billing']['billing_company']['priority'] = 3;
    $fields['billing']['billing_company']['class'] = array('form-row-first');

    $fields['billing']['company_code']['priority'] = 4;
    $fields['billing']['company_code']['class'] = array('form-row-last');

    $fields['billing']['billing_email']['priority'] = 5;
    $fields['billing']['billing_email']['class'] = array('form-row-first');

    $fields['billing']['billing_phone']['class'] = array('form-row-last');

    $fields['billing']['billing_state']['priority'] = 7;

    $fields['billing']['billing_country']['priority'] = 8;
    $fields['billing']['billing_country']['class'] = array('form-row-wide');

    $fields['billing']['billing_city']['priority'] = 9;
    $fields['billing']['billing_city']['class'] = array('form-row-wide');

    $fields['billing']['billing_address_1']['class'] = array('form-row-wide');
    $fields['billing']['billing_address_2']['class'] = array('form-row-wide');
    $fields['billing']['billing_postcode']['class'] = array('form-row-wide');

    return $fields;
}
add_filter("woocommerce_checkout_fields", "custom_override_checkout_fields", 1);
add_filter("woocommerce_billing_fields", "custom_override_checkout_fields", 1);

function filter_billing_fields( $billing_fields ) {

    //if( !is_checkout() ) return $billing_fields;

    //$billing_fields['billing_phone']['required'] = false;
    return $billing_fields;
}
//add_filter( 'woocommerce_billing_fields', 'filter_billing_fields', 20, 1 );

function theme_remove_woocommerce_checkout_fields( $fields ) {

//    unset($fields['billing']['billing_first_name']);
//    unset($fields['billing']['billing_last_name']);
//    unset($fields['billing']['billing_company']);
//    unset($fields['billing']['billing_address_1']);
    unset($fields['billing']['billing_address_2']);
//    unset($fields['billing']['billing_city']);
//    unset($fields['billing']['billing_postcode']);
//    unset($fields['billing']['billing_country']);
//    unset($fields['billing']['billing_state']);
//    unset($fields['billing']['billing_phone']);
//    unset($fields['billing']['billing_email']);

    // remove order comment fields
    unset($fields['order']['order_comments']);

    return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'theme_remove_woocommerce_checkout_fields' );

function theme_remove_woocommerce_billing_fields( $fields ) {

    unset($fields['billing_address_2']);
    //unset($fields['billing_phone']);
    return $fields;
}
add_filter( 'woocommerce_billing_fields' , 'theme_remove_woocommerce_billing_fields' );

/**
 * Add Facebook field to WooCommerce checkout page.
 */

function woocoomerce_custom_checkout_field_register($fields) {
    $fields['billing']['company_code'] = array(
        'label' => __('Įmonės kodas', 'krona'),
        'required' => false,
        'clear' => false,
        'type' => 'text',
        'priority' => 4,
        'class' => array('form-row-last')
    );

    $fields['billing']['delivery_type'] = array(
        'label' => __('Pristatymas', 'krona'),
        'required' => false,
        'clear' => false,
        'type' => 'radio',
        'priority' => 100,
        'class' => array('form-row-wide'),
        'options'         => array(
            'free-pickup'         => 'Nemokamas atsiėmimas',
            'delivery-to-home'    => 'Pristatymas į namus',
        ),
    );

    return $fields;
}
add_filter('woocommerce_checkout_fields', 'woocoomerce_custom_checkout_field_register');

/**
 * Update the order meta with field value.
 */

function woocoomerce_custom_checkout_field( $order_id ) {
    if ( !empty($_POST['company_code']) ) {
        update_post_meta( $order_id, 'company_code', sanitize_text_field( $_POST['company_code'] ) );
    }

    if ( !empty($_POST['delivery_type']) ) {
        update_post_meta( $order_id, 'delivery_type', sanitize_text_field( $_POST['delivery_type'] ) );
    }
}
add_action( 'woocommerce_checkout_update_order_meta', 'woocoomerce_custom_checkout_field' );
add_action( 'woocommerce_billing_update_order_meta', 'woocoomerce_custom_checkout_field' );

function woocoomerce_custom_checkout_field_display_admin($order) {
    echo '<p><strong>'.__('Įmonės kodas').':</strong> <br/>' . get_post_meta( $order->get_id(), 'company_code', true ) . '</p>';
}
add_action( 'woocommerce_admin_order_data_after_billing_address', 'woocoomerce_custom_checkout_field_display_admin');

function theme_product_add_to_cart() {

    $id = $_POST['id'];
    global $woocommerce;

    WC()->cart->add_to_cart($id);
    $product = wc_get_product($id);

    $shipping_class_id = $product->get_shipping_class_id();
    $shipping_class= $product->get_shipping_class();
    $fee = 0;

    if ($shipping_class_id) {
        $flat_rates = get_option("woocommerce_flat_rates");
        $fee = $flat_rates[$shipping_class]['cost'];
    }

    $flat_rate_settings = get_option("woocommerce_flat_rate_settings");
    $fee = ($flat_rate_settings['cost_per_order'] + $fee);
    ?>

    <p>
        <?php printf( __("Jūsų krepšelyje yra %s prekė(-ės).", "krona"), $woocommerce->cart->cart_contents_count ); ?>
    </p>
    <p class="modal-total-cost">
        <?php printf( __("Viso už prekės: %s", "krona"), ($woocommerce->cart->total . ' ' . get_woocommerce_currency_symbol()) ); ?>
    </p>
    <p>
        <?php printf( __("Viso pristatymas: %s", "krona"), ($fee . ' ' . get_woocommerce_currency_symbol()) ); ?>
    </p>
    <p>
        <strong>
            <?php printf( __("Viso: %s (su PVM)", "krona"), ( ($woocommerce->cart->total + $fee ) . ' ' . get_woocommerce_currency_symbol())); ?>
        </strong>
    </p>

    <?php
    exit();
}
add_action('wp_ajax_theme_product_add_to_cart', 'theme_product_add_to_cart');
add_action('wp_ajax_nopriv_theme_product_add_to_cart', 'theme_product_add_to_cart');

function theme_replace_paysera_icon()
{
    return get_template_directory_uri() . '/assets/images/paysera.png';
}
add_filter( 'woocommerce_paysera_icon', 'theme_replace_paysera_icon' );


function theme_woocommerce_get_account_fields()
{
    return apply_filters( 'theme_woocommerce_account_fields', array(
        'billing_phone' => array(
            'type'        => 'text',
            'label'       => __( 'Tel. nr.', 'krona' ),
            'class'       => array('form-row-last'),
            'placeholder' => '',
            'required'    => false,
            'sanitize' => 'wc_clean',
        )
    ));
}

function theme_woocommerce_get_userdata( $user_id, $key )
{
    return get_user_meta( $user_id, $key, true );
}

function theme_woocommerce_print_account_fields()
{
    $fields = theme_woocommerce_get_account_fields();
    $is_user_logged_in = is_user_logged_in();

    foreach ( $fields as $key => $field_args ) {
        $value = null;

        if ( $is_user_logged_in ) {
            $value = get_user_meta( get_current_user_id(), $key, true );
        }

        $value = isset( $field_args['value'] ) ? $field_args['value'] : $value;

        woocommerce_form_field( $key, $field_args, $value );
    }
}
add_action( 'woocommerce_edit_account_form', 'theme_woocommerce_print_account_fields', 10 );

function theme_woocommerce_save_account_fields( $customer_id ) {
    $fields = theme_woocommerce_get_account_fields();
    $sanitized_data = array();

    foreach ( $fields as $key => $field_args ) {
        $sanitize = isset( $field_args['sanitize'] ) ? $field_args['sanitize'] : 'wc_clean';
        $value    = isset( $_POST[ $key ] ) ? call_user_func( $sanitize, $_POST[ $key ] ) : '';

        if ( $key == "billing_phone" ) {
            $sanitized_data[ $key ] = $value;
            continue;
        }

        update_user_meta( $customer_id, $key, $value );
    }

    if ( ! empty( $sanitized_data ) ) {
        $sanitized_data['ID'] = $customer_id;
        wp_update_user( $sanitized_data );
    }
}

add_action( 'woocommerce_created_customer', 'theme_woocommerce_save_account_fields' ); // register/checkout
add_action( 'personal_options_update', 'theme_woocommerce_save_account_fields' ); // edit own account admin
add_action( 'edit_user_profile_update', 'theme_woocommerce_save_account_fields' ); // edit other account admin
add_action( 'woocommerce_save_account_details', 'theme_woocommerce_save_account_fields' ); // edit WC account

add_action( 'woocommerce_save_account_details', 'theme_update_account_details', 12, 1 );
function theme_update_account_details( $user_id )
{
    if( isset( $_POST['billing_phone'] ) )
        update_user_meta( $user_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
}

function theme_woocommerce_save_account_details_required_fields( $required_fields ){
    unset( $required_fields['account_display_name'] );
    return $required_fields;
}
add_filter('woocommerce_save_account_details_required_fields', 'theme_woocommerce_save_account_details_required_fields' );


function theme_woocommerce_add_order_again_to_my_orders_actions( $actions, $order ) {
    if ( $order->has_status( 'completed' ) ) {
        $actions['order-again'] = array(
            'url'  => wp_nonce_url( add_query_arg( 'order_again', $order->id ) , 'woocommerce-order_again' ),
            'name' => __( 'Užsakyti iš naujo', 'krona' )
        );
    }

    return $actions;
}

add_filter( 'woocommerce_my_account_my_orders_actions', 'theme_woocommerce_add_order_again_to_my_orders_actions', 50, 2 );

function theme_woocommerce_endpoint_title( $title, $id ) {
    if ( is_wc_endpoint_url( 'orders' ) && in_the_loop() ) {
        $title = "Užsakymų istorija";
    }

    if ( is_wc_endpoint_url( 'edit-account' ) && in_the_loop() ) {
        $title = "Jūsų asmeniniai duomenys";
    }

    return $title;
}
add_filter( 'the_title', 'theme_woocommerce_endpoint_title', 10, 2 );

/**
 * Update cart totals.
 */
function woocommerce_refresh_cart_count($fragments) {
    global $woocommerce;
    $fragments['.nav-cart span'] = $woocommerce->cart->get_cart_total();
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'oocommerce_refresh_cart_count');

function update_cart_item_count() {
    global $woocommerce;
    wp_send_json(wc()->cart->get_cart_contents_count());
}
add_action('wp_ajax_update_cart_item_count', 'update_cart_item_count');
add_action('wp_ajax_nopriv_update_cart_item_count', 'update_cart_item_count');

function websites_depot_order_fragments_split_shipping($order_fragments)
{
    ob_start();
    websites_depot_woocommerce_order_review_shipping_split();
    $websites_depot_woocommerce_order_review_shipping_split = ob_get_clean();

    $order_fragments['.websites-depot-checkout-review-shipping-table'] = $websites_depot_woocommerce_order_review_shipping_split;
    return $order_fragments;
}
add_filter('woocommerce_update_order_review_fragments', 'websites_depot_order_fragments_split_shipping', 10, 1);

function websites_depot_woocommerce_order_review_shipping_split( $deprecated = false ) {
    wc_get_template( 'checkout/shipping-order-review.php', array( 'checkout' => WC()->checkout() ) );
}

function websites_depot_move_new_shipping_table() {
    wc_get_template( 'checkout/shipping-order-review.php', array( 'checkout' => WC()->checkout() ) );
}
add_action('theme_checkout_shipping_data', 'websites_depot_move_new_shipping_table', 5);

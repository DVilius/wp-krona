<?php
get_header(); ?>

	<div class="wrap">
		<?php if ( have_posts() ) :

			get_template_part( 'loop' );

		else :

			get_template_part( 'content', 'none' );

		endif; ?>
	</div>

<?php
get_footer();